﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TesteClient
{
    class Program
    {
        static void Main(string[] args)
        {
            LogisticaSAService.LogisticaSAServiceClient proxy = new LogisticaSAService.LogisticaSAServiceClient();

            decimal result = proxy.GetShippingRate(3);

            ShippingAllServiceClient.shippingallservicePortTypeClient proxyphp = new ShippingAllServiceClient.shippingallservicePortTypeClient();

            decimal resultphp = Convert.ToDecimal(proxyphp.GetShippingRate(3));

            Console.WriteLine("LogisticaSA: " + result);
            Console.WriteLine("ShippingAll: " + resultphp);
        }
    }
}
