﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34003
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TesteClient.ShippingAllServiceClient {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="urn:shippingallservice", ConfigurationName="ShippingAllServiceClient.shippingallservicePortType")]
    public interface shippingallservicePortType {
        
        [System.ServiceModel.OperationContractAttribute(Action="urn:shippingallservice#GetShippingRate", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(Style=System.ServiceModel.OperationFormatStyle.Rpc, SupportFaults=true, Use=System.ServiceModel.OperationFormatUse.Encoded)]
        float GetShippingRate(int NumberOfItems);
        
        [System.ServiceModel.OperationContractAttribute(Action="urn:shippingallservice#GetShippingRate", ReplyAction="*")]
        System.Threading.Tasks.Task<float> GetShippingRateAsync(int NumberOfItems);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface shippingallservicePortTypeChannel : TesteClient.ShippingAllServiceClient.shippingallservicePortType, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class shippingallservicePortTypeClient : System.ServiceModel.ClientBase<TesteClient.ShippingAllServiceClient.shippingallservicePortType>, TesteClient.ShippingAllServiceClient.shippingallservicePortType {
        
        public shippingallservicePortTypeClient() {
        }
        
        public shippingallservicePortTypeClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public shippingallservicePortTypeClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public shippingallservicePortTypeClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public shippingallservicePortTypeClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public float GetShippingRate(int NumberOfItems) {
            return base.Channel.GetShippingRate(NumberOfItems);
        }
        
        public System.Threading.Tasks.Task<float> GetShippingRateAsync(int NumberOfItems) {
            return base.Channel.GetShippingRateAsync(NumberOfItems);
        }
    }
}
