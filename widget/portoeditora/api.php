<?php

/**
 * ARQSI:
 * Bruno Flávio - 1040865 || Luís Teixeira - 1050510
 */

/**
 * Simple API to access the publisher database contained in the database.xml file
 * and return to our widget.
 */
ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_NOTICE);

/**
 * Configure the output content type:
 */
header('Content-Type: application/json ; charset=utf-8');

/**
 * Database XML location:
 */
$dbFile = "database.xml";

//Parse the incoming filters:
$filters = parseFilters($_GET);

//if there is nothing to seach for terminate the execution.
if (empty($filters))
    die();


//Load the database
$db = loadDB($dbFile);

$return;

//if we're listing categories:
if ($filters['category'] == "all") {
    $return = listCategories($db);
} else {
    //execute the intended query
    $return = queryDB($db, $filters);
}
//output the data:
print(json_encode($return));

/**
 * Load a XML database and return an associative array.
 * @param type $path
 * @return type
 */
function loadDB($path) {
    $dom = simplexml_load_file($path);

    $content = json_decode(json_encode($dom), true);
    return $content;
}

function listCategories($db) {
    $dbCategories = $db['category'];
    $categories = array();
    foreach ($dbCategories as $dbCategory) {
        $categories[] = $dbCategory['@attributes']['id'];
    }
    return $categories;
}

/**
 * Query the array database with the given filters:
 */
function queryDB($db, $filters) {
    $books = filterByCategory($db, $filters['category']);
    $books = filterByTitle($books, $filters['title']);
    return $books;
}

function filterByCategory($db, $category) {
    $dbCategories = $db['category'];
    $books = array();
    foreach ($dbCategories as $dbCategory) {
        if (!isset($category) ||
                $dbCategory['@attributes']['id'] == $category ||
                $category == "all"
        ) {
            //multiple books:
            if (isset($dbCategory['book']['0'])) {
                foreach ($dbCategory['book'] as $book) {
                    $book['category'] = $dbCategory['@attributes']['id'];
                    $books[] = $book;
                }
            } else {
                $book = $dbCategory['book'];
                $book['category'] = $dbCategory['@attributes']['id'];
                $books[] = $book;
            }
        }
    }
    return $books;
}

function filterByTitle($dbBooks, $title) {

    $books = array();
    foreach ($dbBooks as $dbBook) {
        if (!isset($title) ||
                stristr($dbBook['title'], $title) !== FALSE
        ) {
            $books[] = $dbBook;
        }
    }
    return $books;
}

/**
 * Parses the incoming filters:
 */
function parseFilters($incommingParameters) {
    $filters = array();

    if (isset($incommingParameters)) {

        if (isset($incommingParameters['title'])) {
            $filters['title'] = $incommingParameters['title'];
        }

        if (isset($incommingParameters['category'])) {
            $filters['category'] = $incommingParameters['category'];
        }
    }

    return $filters;
}
