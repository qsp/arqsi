/**
 * ARQSI:
 * Bruno Flávio - 1040865 || Luís Teixeira - 1050510
 */



//when the widget is called from within a webpage, create a new div with the contents:

//salvar a tag onde estamos neste momento:
var commands = {
    categories:     "getcategories",
    title:          "findbooksbyname",
    booksByCategory:"listbooksbycategory",
    getBookDetails: "getbookdetails"
};
var loadingGifPath = "img/loading.gif";
var pathToWebService = "service/index.php?json=";
var pathToStaticService = "service/static.php?json=";
var booksWidgetId = "books-widget";
var scripts = document.getElementsByTagName('script');
var widgetJsTag = scripts[ scripts.length - 1 ];
var placeholder;
var detailsDiv;
var widgetDiv;
var searchMan;

function loadWidget() {
    objAjax = new Ajax();
    objAjax.setOnReady(init);
    objAjax.makeRequest("GET", "views/widget.html");
}

function init(xmlhttp) {
    placeholder = document.createElement("div");
    placeholder.setAttribute("id", "wrapper");
    placeholder.innerHTML = xmlhttp.responseText;

    var scriptElement = document.createElement("script");
    scriptElement.setAttribute("type", "text/javascript");
    scriptElement.setAttribute("src", "js/jquery-1.10.2.min.js");
    widgetJsTag.parentNode.insertBefore(scriptElement, widgetJsTag);

    widgetJsTag.parentNode.insertBefore(placeholder, widgetJsTag.nextSibling);
    widgetDiv = document.getElementById(booksWidgetId);

//    searchDiv = document.createElement("div");
//    searchDiv.setAttribute("id","search");
//    searchDiv.className += " inset";
//    widgetDiv.appendChild(searchDiv);
    
    var searchMethod = document.getElementById("search_method");
    //var titleInput = document.getElementById();
    var catSelect = new CategoriesSelect(searchMethod);
    var results = new ResultList(catSelect, widgetDiv);
//    catSelect.makeRequest();
    searchMan = new SearchManager(results, catSelect); 
    searchMan.init();
}

var Book = function() {
    /* Html Elements*/
    this.loadDiv;
    this.imageDiv;
    this.descriptionDiv;
    this.ratingDiv;
    this.previewDiv;
    this.backDiv;
    this.loadingDiv;

    /* Book attributes*/
    var title;
    var author;
    var isbn;
    var publisher;
    var year;
    var categories = [];
    var featured;
    var description;
    var imgUrl;
    var avgRate;
    var previewUrl;
    var infoUrl;
    var complete = false;

    Book.prototype.buildHTML = function() {
        this.loadDiv = document.createElement("div");
        this.loadDiv.setAttribute("typeof","ed:book");
        this. loadDiv.setAttribute("resource","#" + this.isbn);
        var titleDiv = document.createElement("div");

        var bookIco = document.createElement("span");
        bookIco.className += " icon-book font-gray";
        titleDiv.appendChild(bookIco);

        if (this.featured) {
            var featuredIco = document.createElement("span");
            featuredIco.setAttribute("title", "Novidade!");
            featuredIco.className += " icon-bookmark font-red";
            titleDiv.appendChild(featuredIco);
        }
        var titleSpan = document.createElement("span");
        titleSpan.setAttribute("property","ed:title");
        var textNode = document.createTextNode(this.title);
        titleSpan.appendChild(textNode);
        titleDiv.appendChild(titleSpan);
        titleDiv.setAttribute("class", "title");
        this.loadDiv.appendChild(titleDiv);
        var authorDiv = document.createElement("div");
        authorDiv.setAttribute("property","ed:author");
        textNode = document.createTextNode(this.author);
        authorDiv.appendChild(textNode);
        authorDiv.setAttribute("class", "author");
        this.loadDiv.appendChild(authorDiv);
        var publisherDiv = document.createElement("div");
        publisherDiv.setAttribute("property","ed:publisher");
        textNode = document.createTextNode(this.publisher);
        publisherDiv.appendChild(textNode);
        publisherDiv.setAttribute("class", "publisher");
        this.loadDiv.appendChild(publisherDiv);
        this.loadDiv.setAttribute("class", "book");


        this.loadDiv.onclick = this.makeRequest.bind(this);
        return this.loadDiv;
    };

    Book.prototype.makeRequest = function() {
        if (!this.complete) {
            this.buildDetailsDiv();
            var ajaxObj = new Ajax();
            ajaxObj.setOnReady(this.onReady.bind(this));
            var options = {
                "isbn": this.isbn,
                "publisher": this.publisher
            };
            var json = {
                "command": commands.getBookDetails,
                "options": options
            };
            var requestPath = pathToWebService + JSON.stringify(json);
            ajaxObj.makeRequest("GET", requestPath);
        } else {
            this.buildDetailsDiv();
        }

    };

    Book.prototype.onReady = function() {
        xmlDocument = xmlhttp.responseXML;
        parser = new XmlParser();
        parser.getDetails(this, xmlDocument);
        this.complete = true;
        this.buildDetailsDiv();
    };

    Book.prototype.buildDetailsDiv = function() {
        detailsDiv.innerHTML = "";
        detailsDiv.className += " inset";

        /* Image */
        this.imageDiv = document.createElement("div");
        this.imageDiv.setAttribute("id", "image-div");
        this.imageDiv.setAttribute("class", "center");
        if (this.imageUrl) {
            this.imageDiv.appendChild(this.buildImageElement());
        }
        detailsDiv.appendChild(this.imageDiv);

        /* Title */
        var titleDiv = document.createElement("div");
        titleDiv.setAttribute("class", "title center");
        var textNode = document.createTextNode(this.title);
        titleDiv.appendChild(textNode);
        detailsDiv.appendChild(titleDiv);

        /* Featured */
        if (this.featured) {
            var featuredIco = document.createElement("span");
            featuredIco.setAttribute("title", "Novidade!");
            featuredIco.className += " icon-bookmark font-red";
            titleDiv.appendChild(featuredIco);
        }

        /* Author */
        var authorDiv = document.createElement("div");
        textNode = document.createTextNode(this.author);
        authorDiv.appendChild(textNode);
        authorDiv.setAttribute("class", "author center");
        detailsDiv.appendChild(authorDiv);

        /* Publisher */
        var publisherDiv = document.createElement("div");
        textNode = document.createTextNode(this.publisher);
        publisherDiv.appendChild(textNode);
        publisherDiv.setAttribute("class", "publisher center");
        detailsDiv.appendChild(publisherDiv);

        /* Year */
        var yearDiv = document.createElement("div");
        textNode = document.createTextNode(this.year);
        yearDiv.appendChild(textNode);
        yearDiv.setAttribute("class", "year center");
        detailsDiv.appendChild(yearDiv);

        /* Rating */
        this.ratingDiv = document.createElement("div");
        if (this.avgRate) {
            this.ratingDiv.appendChild(this.buildRatingIcons());
        }
        detailsDiv.appendChild(this.ratingDiv);

        /* Description*/
        this.descriptionDiv = document.createElement("div");
        if(this.description){
            this.descriptionDiv.setAttribute("class", "description center");
            this.descriptionDiv.appendChild(this.getDescriptionTextNode());
        }
        
        detailsDiv.appendChild(this.descriptionDiv);
        
        /* Preview Link*/
        this.previewDiv = document.createElement("div");
        if (this.previewUrl) {
            this.previewDiv.appendChild(this.buildPreviewLink());
        }
        detailsDiv.appendChild(this.previewDiv);

        /* Back Button*/
        if(this.complete){
            this.constructBackButton();
        }else{
            this.buildLoadingDiv();
        }
        slide(this.loadDiv.parentNode, detailsDiv, "left");
    };

    Book.prototype.back = function() {
        console.log(this.loadDiv.parentNode);
        slide(detailsDiv, this.loadDiv.parentNode, "right");
    };

    Book.prototype.buildImageElement = function() {
        var img = document.createElement("img");
        img.setAttribute("src", this.imageUrl);
        img.setAttribute("id", "book-image");
        return img;
    };
    
    Book.prototype.buildRatingIcons = function(){
        var strTxt = "";
        this.ratingDiv.className += " icon-rating";
        var rating = parseFloat(this.avgRate);
        var fullStars = Math.floor(rating / 1);
        for (var i = 0; i < fullStars; i++) {
            strTxt += "1";
        }
        var halfStar = rating % 1;
        if (halfStar >= 0.5) {
            strTxt += "2";
            fullStars++;
        }

        for (var i = fullStars; i < 5; i++) {
            strTxt += "3";
        }
        var txtNode = document.createTextNode(strTxt);
        return txtNode;
    };
    
    Book.prototype.getDescriptionTextNode = function(){
        textNode = document.createTextNode(this.description);
        return textNode;
    };
    
    Book.prototype.buildPreviewLink = function(){
        var prevLink = document.createElement("a");
        prevLink.setAttribute("href", this.previewUrl);
        prevLink.setAttribute("target", "_blank");
        textNode = document.createTextNode("Preview in Google Books");
        prevLink.appendChild(textNode);
        prevLink.className += " book-links";
        return prevLink;
    };
    
    Book.prototype.constructBackButton = function(){
        if(this.loadingDiv){
            //detailsDiv.removeChild(this.loadingDiv);
            this.loadingDiv = undefined;
        }
        this.backDiv = document.createElement("div");
        this.backDiv.className += " back-button";
        var spanArrow = document.createElement("span");
        spanArrow.className += " icon-arrow-left";
        this.backDiv.appendChild(spanArrow);
        var txtNode = document.createTextNode("Back");
        this.backDiv.appendChild(txtNode);
        this.backDiv.onclick = this.back.bind(this);
        detailsDiv.appendChild(this.backDiv);
    };
    
    Book.prototype.buildLoadingDiv = function(){
        this.loadingDiv = document.createElement("div");
        this.loadingDiv.className += " loading";
        var img = document.createElement("img");
        img.setAttribute("src", loadingGifPath);
        this.loadingDiv.appendChild(img);
        detailsDiv.appendChild(this.loadingDiv);
    };
};

var LoadMoreObj = function(position,category){
    var parent;
    this.category = category;
    this.position = position;
    var loadDiv;
    
    LoadMoreObj.prototype.onReady = function(){
        var parser = new XmlParser();
        var xmlDocument = xmlhttp.responseXML;
        var books = parser.getBooks(xmlDocument);
        console.log(this.parent);
        this.parent.removeChild(this.loadDiv);
        if(books.length > 0){
            for (var i = 0; i < books.length; i++) {
                var bookElement = books[i].buildHTML();
                this.parent.appendChild(bookElement);               
            }
            var loadMoreDiv = this.buildHTML();
            this.parent.appendChild(loadMoreDiv);
        }
    };
    
    LoadMoreObj.prototype.makeRequest = function(){
        console.log("Load More: " + this.position +","+ this.category);
        
        this.loadDiv.innerHTML = "";
        var img = document.createElement("img");
        img.setAttribute("src","/img/loading.gif");
        img.className = "loading";
        this.loadDiv.appendChild(img);
        
        var ajaxObj = new Ajax();
        ajaxObj.setOnReady(this.onReady.bind(this));
        
        var json;
        json = {
            "command": commands.booksByCategory,
            "options": {"category": this.category}
        };

        if (searchMan.isFiltering === true) {
            var limits = {
                "max": "",
                "global": ""
            };
            limits.max = searchMan.numElem;
            switch (searchMan.filterBy) {
                case 'total':
                    limits.global = true;
                    break;
                case 'publisher':
                    limits.global = false;
                    break;
            }
            json.options["limits"] = limits;
        }
        
        var page = {
                "first":this.position,
                "items":"5"
        };
        json.options["page"] = page;
        this.position += 5;
        
        var requestPath = pathToWebService + JSON.stringify(json);
        ajaxObj.makeRequest("GET", requestPath);
    };
    
    LoadMoreObj.prototype.buildHTML = function() {
        this.loadDiv = document.createElement("div");
        this.loadDiv.className = "book";
        var titleDiv = document.createElement("div");

        var titleSpan = document.createElement("span");
        var textNode = document.createTextNode("Carregar Mais");
        titleSpan.appendChild(textNode);
        titleDiv.appendChild(titleSpan);
        titleDiv.setAttribute("class", "title");
        this.loadDiv.appendChild(titleDiv);

        this.loadDiv.onclick = this.makeRequest.bind(this);
        return this.loadDiv;
    };
};

var ResultList = function(categoriesSelect, parent) {
    this.categoriesSelect = categoriesSelect;
    this.parent = parent;
    this.resultsDiv = null;
    
    var position;
    var loadMoreObj;

    ResultList.prototype.makeRequest = function(param){
        //console.log(this.categoriesSelect);  
        searchMan.deactivatePlus();
        var ajaxObj = new Ajax();
        ajaxObj.setOnReady(this.onReady.bind(this));
        
        var json;
        if(searchMan.searchBy === 'category'){
            this.position = 0;
            json = {
                "command": commands.booksByCategory,
                "options": {"category": param}
            };
            
            if (searchMan.isFiltering === true) {
                var limits = {
                    "max": "",
                    "global": ""
                };
                limits.max = searchMan.numElem;
                switch (searchMan.filterBy) {
                    case 'total':
                        limits.global = true;
                        break;
                    case 'publisher':
                        limits.global = false;
                        break;
                }
                json.options["limits"] = limits;
            }
        }else{
            json = {
                "command": commands.title,
                "options": {"title":param}
            };
        }
        
        var page = {
                "first":this.position,
                "items":"5"
        };
        json.options["page"] = page;
        this.position += 5;
        this.loadMoreObj = new LoadMoreObj(this.position,param);
        var requestPath = pathToWebService + JSON.stringify(json);
        ajaxObj.makeRequest("GET", requestPath);
        var staticPath = pathToStaticService + JSON.stringify(json);
        document.getElementById("rdfAlink").href=staticPath;
    };

    ResultList.prototype.onReady = function() {
        var parser = new XmlParser();
        var xmlDocument = xmlhttp.responseXML;
        var books = parser.getBooks(xmlDocument);
        this.buildHTML(books);
        searchMan.activatePlus();
    };

    ResultList.prototype.buildHTML = function(books) {
        if (this.resultsDiv !== null) {
            this.resultsDiv.innerHTML = "";
        } else {
            detailsWrapperDiv = document.createElement("div");
            detailsWrapperDiv.className += " details-wrapper";
            this.parent.appendChild(detailsWrapperDiv);
            detailsDiv = document.createElement("div");
            detailsDiv.setAttribute("id", "details");
            this.resultsDiv = document.createElement("div");
            this.resultsDiv.setAttribute('id', 'results');
            this.resultsDiv.className += " inset";
            detailsWrapperDiv.appendChild(this.resultsDiv);
        }
        this.loadMoreObj.parent = this.resultsDiv;
        for (var i = 0; i < books.length; i++) {
            var bookElement = books[i].buildHTML();
            this.resultsDiv.appendChild(bookElement);
        }
        if(searchMan.searchBy === 'category'){
            var loadMoreDiv = this.loadMoreObj.buildHTML();
            this.resultsDiv.appendChild(loadMoreDiv);
        }
        if (this.resultsDiv.style.display === "none") {
            slide(detailsDiv, this.resultsDiv, "right");
        }
    };

    categoriesSelect.addCallback(this.makeRequest.bind(this));
};

var CategoriesSelect = function(parent) {

//    if (!(parent instanceof HTMLElement)) {
//        throw("Wrong type Parameter(s)\n parent must be of type HTMLElement");
//    }

    /*The parent of the select element*/
    this.parent = parent;
    this.selectElement = null;
    this.callbacks = [];
    
    this.line = document.createElement("div");
    this.line.setAttribute("id", "select_group");
    this.line.className += " left_item";
    this.parent.appendChild(this.line);
    

    CategoriesSelect.prototype.makeRequest = function() {
        var ajaxObj = new Ajax();
        ajaxObj.setOnReady(this.onReady.bind(this));
        var json = {"command": commands.categories};
        var requestPath = pathToWebService + JSON.stringify(json);
        ajaxObj.makeRequest("GET", requestPath);
    };

    CategoriesSelect.prototype.addCallback = function(observer) {
        this.callbacks.push(observer);
    };

    /*OnReady functions for the Ajax request*/
    CategoriesSelect.prototype.onReady = function() {
        if (this.selectElement !== null) {
            /*TODO:Apagar select existente*/
        }
        var parser = new XmlParser();
        var xmlDocument = xmlhttp.responseXML;
        var categoriesArray = parser.getCategories(xmlDocument);
        this.selectElement = buildHtml(categoriesArray);
        this.selectElement.onchange = this.notifyCallbacks.bind(this);

        var label = document.createElement("label");
        var text = document.createTextNode("Categoria:");
        label.appendChild(text);
        this.line.appendChild(label);
        this.line.appendChild(this.selectElement);        
        this.notifyCallbacks(); // para a  lista nao aparecer vazia, mas sim com os livros da primeira categoria
    };

    CategoriesSelect.prototype.notifyCallbacks = function() {
        searchMan.hideSearchOpts();
        var selectedIndex = this.selectElement.selectedIndex;
        var selectedCategory = this.selectElement.options[selectedIndex].text;
        for (var i = 0; i < this.callbacks.length; i++) {
            /*TODO:Chamar função dos observers(Falta definir essa função)*/
            this.callbacks[i](selectedCategory);
        }
    };

    /*Function to create the html element*/
    var buildHtml = function(categoriesArray) {
//        if (!(categoriesArray instanceof Array)) {
//            throw("Wrong type Parameter(s)\nCategoriesArray must be of type Array");
//        }
        var select = document.createElement("select");
        select.setAttribute("id", "catselector");
        for (var i = 0; i < categoriesArray.length; i++) {
            var optionElement = document.createElement("option");
            var item = categoriesArray[i];
            optionElement.setAttribute("value", item);
            select.appendChild(optionElement);
            text = document.createTextNode(item);
            optionElement.appendChild(text);
        }
        return select;
    };
};

var SearchManager = function(resultList,categorySelect){
    
    this.resultList = resultList;
    this.categorySelect = categorySelect;
    this.isClosed = true;
    this.title = "";
    this.searchBy = 'category';
    this.numElem = 5;
    this.isFiltering = false;
    this.filterBy = 'total';
    
    SearchManager.prototype.init = function(){
        this.catDiv = document.getElementById("select_group");
        
        this.titleDiv = document.getElementById("title_group");
        this.titleInput = document.getElementById("title_input");
        this.titleInput.onchange = this.titleListener.bind(this);
        
        this.plusDiv = document.getElementById("search_menu");
        this.plusDiv.onclick = this.onPlusClicked.bind(this);

        this.searchByDiv = document.getElementById("filter_searchby");
        this.nameToggle = document.getElementById("name_toggle");
        this.nameToggle.onclick = this.searchByListener.bind(this);
        this.catToggle = document.getElementById("cat_toggle");
        this.catToggle.onclick = this.searchByListener.bind(this);

        this.limitDiv = document.getElementById("filter_limit");
        this.yesToggle = document.getElementById("yes_toggle");
        this.yesToggle.onclick = this.filterListener.bind(this);
        this.noToggle = document.getElementById("no_toggle");
        this.noToggle.onclick = this.filterListener.bind(this);
        
        this.numInput = document.getElementById("num_input");
        this.numInput.onkeyup = this.numListener.bind(this);
        this.totalToggle = document.getElementById("total_toggle");
        this.publisherToggle = document.getElementById("publisher_toggle");
        
        this.start(this.categorySelect);
        
     };
     
    SearchManager.prototype.deactivatePlus = function(){
        this.plusDiv.innerHTML = "";
        var img = document.createElement("img");
        img.setAttribute("src", loadingGifPath);
        this.plusDiv.appendChild(img);
        this.plusDiv.style.cursor = "default";
        this.plusDiv.className = "";
        this.plusDiv.onclick = null;
    };
    
    SearchManager.prototype.activatePlus = function(){
        this.plusDiv.innerHTML = "";
        this.plusDiv.style.cursor = "pointer";
        this.plusDiv.className = "icon-plus";
        this.plusDiv.onclick = this.onPlusClicked.bind(this);
    };
    
    SearchManager.prototype.start = function(){
        this.titleDiv.style.display = "none";
        this.searchByDiv.style.display = "none";
        this.limitDiv.style.display = "none";
        
        this.categorySelect.makeRequest();
    };
    
    SearchManager.prototype.hideSearchOpts = function(){
        this.plusDiv.className = "icon-plus";
        this.isClosed = true;
        this.searchByDiv.style.display = "none";
        this.limitDiv.style.display = "none";
    };
    
    SearchManager.prototype.onPlusClicked = function(){
        if(this.isClosed){
            this.plusDiv.className = "icon-plus rotate45";
            this.isClosed = false;
            this.searchByDiv.style.display = "table";
            if(this.searchBy === 'category') {
                this.limitDiv.style.display = "table";
            }
        }else{
            this.isClosed = true;
            this.plusDiv.className = "icon-plus";
            this.searchByDiv.style.display = "none";
            this.limitDiv.style.display = "none";
        }
    };
    
    SearchManager.prototype.searchByListener = function(event){
       switch (event.srcElement) {
           case this.nameToggle:
               this.searchBy = 'title';
               //ui:
               this.nameToggle.className = " toggle selected";
               this.catToggle.className = " toggle";
               this.catDiv.style.display = "none";
               this.titleDiv.style.display = "block";
               this.limitDiv.style.display = "none";
               
               break;
           case this.catToggle:
               this.searchBy = 'category';
               //ui:
               this.catToggle.className = " toggle selected";
               this.nameToggle.className = " toggle";
               this.titleDiv.style.display = "none";
               this.catDiv.style.display = "block";
               this.limitDiv.style.display = "table";
               
               break;
       }
    }; 
    
    SearchManager.prototype.filterListener = function(event){
       switch (event.target) {
           case this.yesToggle:
               this.isFiltering = true;
               this.filterBy='total';
               //ui:
               this.yesToggle.className = " toggle selected";
               this.noToggle.className = " toggle";
               this.numInput.removeAttribute("disabled");
               this.numInput.value = this.numElem;
               this.totalToggle.className = " toggle selected";
               this.publisherToggle.className = " toggle";
               this.publisherToggle.onclick = this.filterByListener.bind(this);
               this.totalToggle.onclick = this.filterByListener.bind(this);
               
               break;
           case this.noToggle:
               this.isFiltering = false;               
               //ui:
               this.noToggle.className = " toggle selected";
               this.yesToggle.className = " toggle";
               this.numInput.setAttribute("disabled","");
               this.numInput.value = "";
               this.totalToggle.className = " toggle disabled";
               this.publisherToggle.className = " toggle disabled";
               this.publisherToggle.onclick = null;
               this.totalToggle.onclick = null;
               break;
       }
    }; 
    
    SearchManager.prototype.filterByListener = function(event){
       switch (event.target) {
           case this.publisherToggle:
               this.filterBy='publisher';
               //ui:
               this.publisherToggle.className = " toggle selected";
               this.totalToggle.className = " toggle";
               
               break;
           case this.totalToggle:
               this.filterBy = 'total';
               //ui:
               this.totalToggle.className = " toggle selected";
               this.publisherToggle.className = " toggle";
               
               break;
       }
    };
    
    SearchManager.prototype.titleListener = function(){
        this.title = this.titleInput.value;
        this.hideSearchOpts();
        this.resultList.makeRequest(this.title);
    };
    
    SearchManager.prototype.numListener = function(event){
        if(event.which === 13){
            this.numElem = this.numInput.value;
            this.categorySelect.selectElement.onchange();
        }
    };
    
};

var Ajax = function() {
    var onReady;
    var onError;
    var onWait;

    if (window.XMLHttpRequest)
    { //IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    }
    else
    { //IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    Ajax.prototype.setOnReady = function(onReadyFunc) {
        onReady = onReadyFunc;
    };

    Ajax.prototype.setOnError = function(onErrorFunc) {
        onError = onErrorFunc;
    };

    Ajax.prototype.setOnWait = function(onWaitFunc) {
        onWait = onWaitFunc;
    };

    Ajax.prototype.makeRequest = function(metodo, url) {
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState === 4 && xmlhttp.status === 200 && onReady !== undefined && onReady !== null) {
                onReady(xmlhttp);
            } else if (xmlhttp.readyState === 4 && onError !== undefined && onError !== null) {
                onError();
            } else if (xmlhttp.readyState !== 1 && onWait !== undefined && onWait !== null) {
                console.log("Call onwait function");
                onWait();
            }
        };
        xmlhttp.open(metodo, url, true);
        xmlhttp.send();
    };
};

var XmlParser = function() {

    Array.prototype.stringInArray = function(string) {
        for (var i = 0; i < this.length; i++) {
            if (this[i] === string)
                return true;
        }
        return false;
    };

    Array.prototype.insertStringIfNotExists = function(string) {
        if (!this.stringInArray(string)) {
            this.push(string);
        }
    };

    XmlParser.prototype.getCategories = function(xmlDocument) {
        var arr = [];
        var categorieElements = xmlDocument.getElementsByTagName("category");
        for (var i = 0; i < categorieElements.length; i++) {
            categorieElement = categorieElements[i];
            textNode = categorieElement.childNodes[0];
            text = textNode.nodeValue;
            arr.insertStringIfNotExists(text);
        }
        return arr;
    };

    XmlParser.prototype.getBooks = function(xmlDocument) {
        var arr = [];
        var bookElements = xmlDocument.getElementsByTagName("book");
        for (var i = 0; i < bookElements.length; i++) {
            arr.push(this.parseBook(bookElements[i]));
        }
        return arr;
    };

    XmlParser.prototype.parseBook = function(xmlElement) {
        var book = new Book();

        var titleElement = xmlElement.getElementsByTagName("title");
        book.title = titleElement[0].childNodes[0].nodeValue;
        var authorElement = xmlElement.getElementsByTagName("author");
        book.author = authorElement[0].childNodes[0].nodeValue;
        var isbnElement = xmlElement.getElementsByTagName("isbn");
        book.isbn = isbnElement[0].childNodes[0].nodeValue;
        var publisherElement = xmlElement.getElementsByTagName("publisher");
        book.publisher = publisherElement[0].childNodes[0].nodeValue;
        var yearElement = xmlElement.getElementsByTagName("year");
        book.year = yearElement[0].childNodes[0].nodeValue;
        var categoriesElement = xmlElement.getElementsByTagName("categories");
        book.categories = this.getCategories(categoriesElement[0]);

        book.featured = xmlElement.getAttribute('featured') === "true";

        return book;
    };

    XmlParser.prototype.getDetails = function(book, xmlElement) {
        var descriptionElement = xmlElement.getElementsByTagName("description");
        if (descriptionElement.length > 0) {
            book.description = descriptionElement[0].childNodes[0].nodeValue;
        }

        var avgRateElement = xmlElement.getElementsByTagName("average-rate");
        console.log(avgRateElement);
        if (avgRateElement.length > 0) {
            book.avgRate = avgRateElement[0].childNodes[0].nodeValue;
        }

        var imageElement = xmlElement.getElementsByTagName("image");
        if (imageElement.length > 0) {
            book.imageUrl = imageElement[0].childNodes[0].nodeValue;
        }

        var previewElement = xmlElement.getElementsByTagName("preview-link");
        if (previewElement.length > 0) {
            book.previewUrl = previewElement[0].childNodes[0].nodeValue;
        }

        var infoElement = xmlElement.getElementsByTagName("info-link");
        if (infoElement.length > 0) {
            book.infoUrl = infoElement[0].childNodes[0].nodeValue;
        }

    };
};

var slide = function(toHide, toShow, direction) {    
    var parent = toHide.parentNode;
    //-6 por causa do padding e margin.
    var width = $(parent).width()-6;
    var height = $(parent).height()-6;

    $(toHide).css({
        width: width,
        height: height,
        position: "absolute"
    });

    console.log(toShow);
    parent.appendChild(toShow);
    $(toShow).hide().css({
        width: width,
        height: height,
        position: 'absolute'
    });
    
    if (direction === "left") {
        
        $(toShow).hide().css({
            left: width
        });

        $(toHide).animate({left: -(width)}, 250, function() {
            $(toHide).hide();
            $(toHide).css({
                width: "",
                height: "100%",
                position: "relative",
                left: null
            });
        });


        $(toShow).show().animate({left: 0}, 500, function() {
            $(toShow).css({
                width: "",
                height: "100%",
                position: "relative",
                overflow: "inherit",
                left: null
            });
        });
    }
    if (direction === "right") {

        $(toHide).animate({left: width}, 250, function() {
            $(toHide).hide();
            $(toHide).css('width', '');
            $(toHide).css({
                width: "",
                height: "100%",
                position: "relative",
                right: null
            });
        });


        $(toShow).show().animate({left: 0}, 500, function() {
            $(toShow).css({
                width: "",
                height: "100%",
                overflow: "inherit",
                position: "relative"
            });            
        });
    }
};

window.onload = loadWidget;
