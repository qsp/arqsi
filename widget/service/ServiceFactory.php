<?php
/**
 * ARQSI:
 * Bruno Flávio - 1040865 || Luís Teixeira - 1050510
 */


//Load Service Adapters:
//require_once 'PublisherAdapter/PublisherAdapterFactory.php';
//require_once 'PublisherAdapter/PublisherPhpdev2Adapter.php';
require_once 'PageService.php';
require_once 'GetCategoriesService.php';
require_once 'ListBooksByCategoryService.php';
require_once 'FindBooksByNameService.php';
require_once 'GetBookDetailsService.php';
require_once 'Database/ConfigDatabase.php';

require_once 'PublisherAdapter/autoloader.php';

/**
 * Description of ServiceFactory
 *
 * @author Luis
 */
class ServiceFactory {
    private $listOfAdapters;
    private $previousCategory;
    private $previousLimits;
    private $previousData;
    
    public function __construct() {
        $this->logClient();
        $this->createAdapters();        
    }
    
    /*Inserir aqui funções para a crição dos vários serviços*/
    public function createService($command, $options){
        $this->logRequest($command, $options);
        switch ($command) {
        case "getcategories":
            return new GetCategoriesService($this->listOfAdapters);
        case "listbooksbycategory":
            
            //check if client wants next page of previous data:
            if ($options["category"] === $this->previousCategory
                    &&
                $options["limits"]   === $this->previousLimits){
                return new PageService($this->previousData, $options["page"]);
            }
            
            //reset pageing attributes:
            $this->previousCategory = null;
            $this->previousLimits = null;
            $this->previousData = null;
            
            //new service request:
            $page = new ListBooksByCategoryService(
                                $this->listOfAdapters, 
                                $options["category"], 
                                $options["limits"]);
            
            //check if client wants pageing:
            if (isset($options["page"])){
                $this->previousCategory = $options["category"];
                $this->previousLimits   = $options["limits"];
                $page->setPageMode(true);
                $this->previousData = $page->run();
                return new PageService($this->previousData, $options["page"]);
            }
            
            //no pageing:
            return $page;
        case "findbooksbyname":
            return new FindBooksByNameService($this->listOfAdapters, $options["title"]);
        case "getbookdetails":
            return new GetBookDetailsService($this->listOfAdapters,$options["isbn"],$options["publisher"]);
        default:
             break;
        }
    }

    private function createAdapters(){
        $factory = new PublisherAdapterFactory();
        $adaptersConfig = $this->getAdaptersConfigurations();
        
        foreach ($adaptersConfig as $adapter){
            $adapterObj = $factory->createPublisherAdapter($adapter[0],$adapter[1],$adapter[2]);
            $this->listOfAdapters[]= $adapterObj;
        }
    }
    
    private function getAdaptersConfigurations(){
        $publishersConnectors = array();
        $connection = mysqli_connect(ConfigDatabase::URI, ConfigDatabase::USER, ConfigDatabase::PASSWORD, ConfigDatabase::SCHEMA);
        
        if(mysqli_connect_errno()) {
            echo "Connection Failed: " . mysqli_connect_errno();
            exit();
        }
        
        $statement = $connection->prepare("SELECT name, url, publisher_adapter FROM " . ConfigDatabase::TABLE_CONFIG_PUBLISHERS);
        if($statement){
            $results = array();
            $statement->bind_result($results[0],$results[1],$results[2]);
            $statement->execute();
            while($statement->fetch()){
                $publishersConnectors[] = array($results[0],$results[1],$results[2]);
            }
            $statement->close();
        }
        $connection->close();

        return $publishersConnectors;
    }
    
    //LOGGING RELATED METHODS:
    private function buildDSN () {
        $dsn = 'mysql:';
        $dsn.= 'host='  . ConfigDatabase::URI     . ';';
        $dsn.= 'dbname='. ConfigDatabase::SCHEMA  . ';';
        $dsn.= 'charset=utf8;';
        return $dsn;
    }
    
    private function connect() {
        $db;
        if (!isset($this->db)) {
            try{
                $db = new PDO($this->buildDSN(), ConfigDatabase::USER, ConfigDatabase::PASSWORD);
                $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch(PDOException $ex) {            
                $this->error_log($ex->getMessage());
            }
        }
        return $db;
    }
    
    private function logClient() {
        $db = $this->connect();
        $client;
        if (!isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $client = $_SERVER['REMOTE_ADDR'];
        } else {
            $client = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        $query = array (
                    ':session_id'       => session_id(),
                    ':client_ip'        => $client,
                    ':remote_endpoint'  => $_SERVER['REMOTE_ADDR'],
                );
        
        try {
            $preparedStatement = $db->prepare(
                    'INSERT INTO log_client(session_id,client_ip,remote_endpoint)' .
                    'VALUES(:session_id,:client_ip,:remote_endpoint)'
                    );
            $preparedStatement->execute($query);
            //close connection.
            $db = null;
        } catch(PDOException $ex) {            
            $this->error_log($ex->getMessage());
        }
    }
    
    private function logRequest($command, $options) {
        $db = $this->connect();
        
        $query = array (
                    ':session_id'       => session_id(),
                    ':command'          => print_r($command, TRUE),
                    ':options'          => print_r($options, TRUE),
                );
        
        try {
            $preparedStatement = $db->prepare(
                    'INSERT INTO log_internal(session_id,command,options)' .
                    'VALUES(:session_id,:command,:options)'
                    );
            $preparedStatement->execute($query);
            //close connection.
            $db = null;
        } catch(PDOException $ex) {            
            $this->error_log($ex->getMessage());
        }
    }
    
    private function error_log($message) {
        $trace=debug_backtrace();
        $output = 'SessionFactory::';
        $output .= $trace[1]['function'] . '():';
        $output .= '|' . print_r($message, TRUE) . "|\n";
        file_put_contents('php://stderr', $output);
    }
}
