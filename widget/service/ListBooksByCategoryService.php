<?php
/**
 * ARQSI:
 * Bruno Flávio - 1040865 || Luís Teixeira - 1050510
 */

require_once './model/BooksDOM.php';

class ListBooksByCategoryService {

    private $publishersAdapters;
    private $category;
    private $limits;
    private $pageMode;

    public function __construct($publishersAdapters, $category, $limits) {
        $this->publishersAdapters = $publishersAdapters;
        $this->category = $category;
        $this->limits = $limits;
    }
    
    public function setPageMode($val) {
        $this->pageMode = $val;
    }

    public function run() {
        $booksDOM = new BooksDOM();
        $cuts = array();
        $numberBooks = 0;

        foreach ($this->publishersAdapters as $adapter) {
            $responseDOM = $adapter->listBooksByCategory($this->category, $this->limits["max"]);
            $cuts[] = $numberBooks;
            for ($i = 0; $i < $responseDOM->length(); $i++) {
                if ($booksDOM->addElement($responseDOM->getElement($i))) {
                    $numberBooks++;
                }
            }
        }

        if (isset($this->limits['max']) && isset($this->limits['global'])) {
            if ($this->limits['global'] == true) {
                $tempDOMs = array();

                for ($pub = 0; $pub < count($cuts); $pub++) {
                    $arr = array();
                    $cutLimit = isset($cuts[$pub + 1]) ? $cuts[$pub + 1] : $numberBooks;
                    for ($i = $cuts[$pub]; $i < $cutLimit; $i++) {
                        $arr[] = $booksDOM->getElement($i);
                    }
                    $tempDOMs[] = $arr;
                }


                //position all publishers on first item:
                $pos = array();
                foreach ($tempDOMs as $tempDOM) {
                    $pos[] = 0;
                }

                //reset $booksDOM:
                $booksDOM = new BooksDOM();
                $outputNum = $this->limits["max"] <= $numberBooks ? $this->limits["max"] : $numberBooks;

                //get first books of each publisher until we gather $outputNum amount:
                $item = 0;
                while ($item < $outputNum) {
                    for ($i = 0; $i < count($tempDOMs); $i++) {
                        if ($pos[$i] < count($tempDOMs[$i])) {
                            $booksDOM->addElement($tempDOMs[$i][$pos[$i]]);
                            $pos[$i] ++;
                            $item++;
                            if ($item >= $outputNum) {
                                break;
                            }
                        }
                    }
                }
            }
        }
        if ($this->pageMode) {
            return $booksDOM;
        }
        return $booksDOM->getDOM()->saveXML();
    }

}
