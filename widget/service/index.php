<?php
/**
 * ARQSI:
 * Bruno Flávio - 1040865 || Luís Teixeira - 1050510
 */


ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_NOTICE);

require_once "ServiceFactory.php";

const SESSION_VALIDITY = 3600; // The validity of the session in seconds
$query = json_decode($_REQUEST['json'],true);

$factory = getServiceFactory();

$command = $query['command'];
$options = $query['options'];
$service = $factory->createService($command,$options);

header("Content-type: text/xml");
print $service->run();

function getServiceFactory(){
    $factory = null;
    session_start();
    checkSessionValidity();
    if(empty($_SESSION['EXISTS'])){
        $_SESSION['EXISTS'] = true;
        $_SESSION['TIMESTAMP'] = time();
        
        $factory = new ServiceFactory();        
        
        $_SESSION['SERVICE_FACTORY'] = serialize($factory);
    }else{
        $factory = unserialize($_SESSION['SERVICE_FACTORY']);
        file_put_contents('php://stderr', print_r($factory,TRUE));
    }
    return $factory;
}

function checkSessionValidity(){
    if(isset($_SESSION['TIMESTAMP']) && (time() - $_SESSION['TIMESTAMP'] > SESSION_VALIDITY)){
        session_unset();
        session_destroy();
        session_start();
    }
}
?>