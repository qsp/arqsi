<?php

/**
 * ARQSI:
 * Bruno Flávio - 1040865 || Luís Teixeira - 1050510
 */


/*
 * STATIC VERSION OF A QUERY FOR USE WITH RDFA QUERIES:
 */

header('Content-Type: text/html; charset=utf-8');

$params;
//check if we were given the expected parameter, leave if not:
if (!isset($_REQUEST['json'])) {
    print("Incorrect parameters");
    return;
} else {
    $params = json_decode($_REQUEST['json'],true);
    
    //remove pageing options:
    if(isset($params['options']['page'])) {
        unset($params['options']['page']);
    }
}

//Prepare the url to the service.
$webServer = "http://uvm095.dei.isep.ipp.pt/service/index.php";
$query = json_encode($params);
$location = $webServer . "?json=" . $query;

//gather XML document:
$document;
try {
    $document = DOMDocument::load($location);
}catch (Exception $e) {
    $document = new DOMDocument();
}
        
//print html starting element:
print htmlStart();

//print html head:
print head();

print document($document, $params);

print htmlEnd();

//DONE.

function document(DOMDocument $document, $params) {
    print ('<body>');
    print ('<h1>BookWidget static service</h1>');
    print ('<h2>Query: '.$params["command"].'</h2>');
    print ('<h3>Options: '.print_r($params["options"], TRUE).'</h2>');
    

    $books = $document->getElementsByTagName('book');
    print('<h4>Results:</h4>');
    print('<ol>');
    foreach ($books as $book) {
        //Reading incomming book parameters:
        $parameters = array();
        foreach ($book->childNodes as $parameter) {
            $parameters[$parameter->nodeName] = $parameter;                
        }
        $featured = $book->getAttribute('featured');
        $title  = $parameters['title']->nodeValue;
        $author = $parameters['author']->nodeValue;
        $title  = $parameters['title']->nodeValue;
        $isbn   = $parameters['isbn']->nodeValue;
        $publisher   = $parameters['publisher']->nodeValue;

        print('<li>');
        print('<span resource="#'.$isbn.'" typeof="ed:book">');
        print('<span typeof="property:title">'.$title.'</span>');
        
        print('<ul>');
        print('<li>Author: <span property="ed:author">'.$author.'</span></li>');
        print('<li>ISBN: <span property="ed:isbn">'.$isbn.'</span></li>');
        print('<li>Publisher: <span property="ed:publisher">'.$publisher.'</span></li>');
        print('<li>Featured: <span property="ed:featured">'.$featured.'</span></li>');
        print('</ul>');
        
        print('</span>');
        print('</li>');
    }
    print ('</ol>');
    
    print '</body>';
}

function htmlStart(){
    $output = 
        '<html xmlns:ed="http://www.uvm095.dei.isep.ipp.pt/editoras#"
          xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
          xmlns:h="http://www.w3.org/1999/xhtml"
          xmlns="http://www.w3.org/1999/xhtml">';
    return $output;
}

function htmlEnd(){    
    return '</html>';
}

function head(){
    $output = 
        '<head>
            <title>BooksWidget Static output</title>
            <meta charset="utf-8"></meta>
            <meta name="viewport" content="width=device-width"></meta>
        </head>';
    return $output;
}