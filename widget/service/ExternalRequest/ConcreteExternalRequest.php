<?php
require_once 'ExternalRequest.php';
/**
 * ARQSI:
 * Bruno Flávio - 1040865 || Luís Teixeira - 1050510
 */

/**
 * ExternalRequest.php
 * 
 * This Class has the responsibility of preparing and making a request to a
 * http source.
 *
 */
class ConcreteExternalRequest implements ExternalRequest {

    const XML = 'Accept: application/xml';
    const JSON = 'Accept: application/json';

    private $url;
    private $context;
    private $returnType;
    private $returnedData;
    private $encoding = 'UTF-8';

    /**
     * ExternalRequest - a wrapper to abstract the mechanism of data gathering
     * from a given url.
     * 
     * The $returnType parameter should be one of:
     *  ExternalRequest::XML or ExternalRequest::JSON
     * 
     * @param type $returnType
     */
    public function __construct($returnTypeInput = self::XML,
            $destination = "", ExternalRequest $wrapTo = null) {

        switch ($returnTypeInput) {
            case self::XML:
                $this->returnType = self::XML;
                break;
            case self::JSON:
                $this->returnType = self::JSON;
                break;
            default:
                $this->returnType = self::XML;
        }

        //set default headers:
        $this->context = stream_context_create(
                array(
                    'http' => array(
                        'header' => $this->returnType,
                        'timeout' => 60
                    )
                )
        );
        
        $this->url = $destination;
    }
    
    public function setParameters($parametros) {

        if (!empty($parametros)) {
            $this->url .= "?";
            foreach ($parametros as $key => $param) {
                $this->url .= urlencode($key) . "=" . urlencode($param) . "&";
            }
            rtrim($this->url, '&');
        }
    }

    public function request() {
        
        $this->returnedData = file_get_contents($this->url, false, $this->context);
        return $this->returnedData === false ? false : true;
    }

    private function returnXML($root = null) {
        $rootOpen = $rootClose = "";
        
        if (!is_null($root)) {  //prepare the root tags, if supplied:
            $rootOpen = '<' . $root . '>';
            $rootClose = '</' . $root . '>';
        }
        
        $xmlDocument = new DOMDocument('1.0', 'UTF-8');
        $xmlDocument->loadXML($rootOpen . utf8_encode($this->returnedData) . $rootClose);
        
        return $xmlDocument;
    }

    private function returnJSON($root = null) {
        return $this->returnedData;
    }

    public function getData($root = null) {
        switch ($this->returnType) {
            case self::XML:
                return $this->returnXML($root);
            case self::JSON:
                return $this->returnJSON($root);
        }
        return "ERROR!";
    }

}
