<?php
/**
 * ARQSI:
 * Bruno Flávio - 1040865 || Luís Teixeira - 1050510
 */

require_once 'ExternalRequest.php';
session_start();
/**
 * LoggingExternalRequest
 * Class that will log our request to the external API's.
 *
 * @author Bruno Flávio 1040865
 */
class LoggingExternalRequest implements ExternalRequest {
    
    private $externalRequest;
    private $destination;
    private $parameters;
    private $db;
    
    public function __construct($returnTypeInput = self::XML, $destination = "", ExternalRequest $wrapTo = null) {
        if (!isset($wrapTo)) {
            $wrapTo = new ConcreteExternalRequest($returnTypeInput, $destination);
        }
        $this->destination = $destination;
        $this->externalRequest = $wrapTo;                
    }

    public function getData($root = null) {
        return $this->externalRequest->getData($root);
    }

    public function request() {
        $this->log();
        return $this->externalRequest->request();
    }

    public function setParameters($parameters) {
        $this->parameters = $parameters;
        return $this->externalRequest->setParameters($parameters);
    }

    private function buildDSN () {
        $dsn = 'mysql:';
        $dsn.= 'host='  . ConfigDatabase::URI     . ';';
        $dsn.= 'dbname='. ConfigDatabase::SCHEMA  . ';';
        $dsn.= 'charset=utf8;';
        return $dsn;
    }
    
    private function connect() {
        if (!isset($this->db)) {
            try{
                $this->db = new PDO($this->buildDSN(), ConfigDatabase::USER, ConfigDatabase::PASSWORD);
                $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch(PDOException $ex) {            
                $this->error_log($ex->getMessage());
            }
        }
    }
    
    private function log() {
        $this->connect();

        $query = array (
                    ':session_id' => session_id(),
                    ':url'        => $this->destination,
                    ':options'    => print_r($this->parameters, TRUE),
                );
        
        try {
            $preparedStatement = $this->db->prepare(
                    'INSERT INTO log_external(session_id,url,options)' .
                    'VALUES(:session_id,:url,:options)'
                    );
            $preparedStatement->execute($query);
        } catch(PDOException $ex) {            
            $this->error_log($ex->getMessage());
        }
    }
    
    private function error_log($message) {
        $trace=debug_backtrace();
        $output = 'LoggingExternalRequest::';
        $output .= $trace[1]['function'] . '():';
        $output .= '|' . print_r($message, TRUE) . "|\n";
        file_put_contents('php://stderr', $output);
    }
        
}
