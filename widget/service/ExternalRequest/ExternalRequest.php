<?php
/**
 * ARQSI:
 * Bruno Flávio - 1040865 || Luís Teixeira - 1050510
 */


/**
 * ExternalRequest.php
 * 
 * This Class has the responsibility of preparing and making a request to a
 * http source.
 *
 */
interface ExternalRequest {

    /**
     * ExternalRequest - a wrapper to abstract the mechanism of data gathering
     * from a given url.
     * 
     * The $returnType parameter should be one of:
     *  ExternalRequest::XML or ExternalRequest::JSON
     * 
     * @param type $returnType
     */
    public function __construct($returnTypeInput = self::XML,
            $destination = "", ExternalRequest $wrapTo = null);
    
    public function setParameters($parametros) ;

    public function request();

    public function getData($root = null);

}
