<?php
/**
 * ARQSI:
 * Bruno Flávio - 1040865 || Luís Teixeira - 1050510
 */

require_once 'ConcreteExternalRequest.php';
require_once 'LoggingExternalRequest.php';

class ExternalRequestFactory {
    public static function build($returnTypeInput = ConcreteExternalRequest::XML,
            $destination = "", $logging = true){
        
        $concrete = new ConcreteExternalRequest($returnTypeInput, $destination);
        
        if ($logging) {            
            return new LoggingExternalRequest(
                        $returnTypeInput,
                        $destination,
                        $concrete);
        } else {
            return $concrete;
        }
    }
}

?>
