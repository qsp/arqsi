<?php
/**
 * ARQSI:
 * Bruno Flávio - 1040865 || Luís Teixeira - 1050510
 */


/**
 * ListDOM
 * Class que permite criar um objeto DOM semelhante a uma lista
 *
 */
class ListDOM {

    const ENCODING = 'UTF-8';
    const TAG_CATEGORY = 'category';

    private $dom;
    private $root;
    private $elementTag;
    private $elementsArray = array();

    public function __construct($root = 'root', $element = 'element') {
        $this->dom = new \DOMDOcument('1.0', 'UTF-8');
        $this->root = $this->dom->createElement($root);
        $this->elementTag = $element;
    }

    public function getDOM() {
        $this->assembleNodes();
        return $this->dom;
    }
    
    public function getRootTag(){
        return $this->root->nodeName;
    }
    
    public function getElementsTag(){
        return $this->elementTag;
    }
    
    public function count() {
        return count($this->elementsArray);
    }
    
    /**
     * Exports the root element to the specified DOM
     * Returns the root element on the destination DOM.
     * @param type $dom
     */
    public function export(\DOMDocument $dom){
        $this->assembleNodes();
        return $dom->importNode($this->root, TRUE);
    }

    protected function import($element) {
        return $this->dom->importNode($element, TRUE);
    }
    
    public function addElement($element) {        
        $this->elementsArray[] = $element;        
    }

    public function setElements($elements) {
        $this->elementsArray = $elements;
        foreach ($elements as $value) {
            $element = $this->dom->createElement($this->elementTag);
            $this->writeValueToElement($value, $element);
            $this->root->appendChild($element);
        }
    }
    
    protected function writeValueToElement($value, DOMElement $element){
        $element->nodeValue = $value;
    }

    public function length(){
        return sizeof($this->elementsArray);
    }
    
    public function getElement($position){
        $item = null;
        if($position < sizeof($this->elementsArray) || $position > 0){
            $item = $this->elementsArray[$position];
        }
        return $item;
    }
    
    private function assembleNodes() {
        $this->dom->appendChild($this->root);        
        if (!empty($this->elementsArray)) {
            $this->setElements($this->elementsArray);
        }
    }

}
