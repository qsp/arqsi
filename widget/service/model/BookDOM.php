<?php
/**
 * ARQSI:
 * Bruno Flávio - 1040865 || Luís Teixeira - 1050510
 */


class BookDOM {    
    const ENCODING = 'UTF-8';
    const TAG_ROOT = 'book';
    const TAG_TITLE = 'title';
    const TAG_AUTHOR = 'author';
    const TAG_ISBN = 'isbn';
    const TAG_CATEGORIES = 'categories';
    const TAG_CATEGORY = 'category';
    const TAG_PUBLISHER = 'publisher';
    const TAG_YEAR = 'year';
    const TAG_DESCRIPTION = 'description';
    const TAG_AVG_RATE = 'average-rate';
    const TAG_IMAGE = 'image';
    const TAG_PREV_LINK = 'preview-link';
    const TAG_INFO_LINK = 'info-link';
    const ATTR_FEATURED = 'featured';

    private $dom;
    private $book;
    private $title;
    private $author;
    private $isbn;
    private $categories;
    private $categoriesArray = array();
    private $publisher;
    private $year;
    private $featured;
    private $description;
    private $avgRating;
    private $image;
    private $previewLink;
    private $infoLink;

    public function __construct() {
        $this->dom = new \DOMDOcument('1.0', 'UTF-8');
        $this->book = $this->dom->createElement(self::TAG_ROOT);
    }

    public function getDOM() {
        $this->assembleNodes();
        return $this->dom;
    }
    
    /**
     * Exports the root element to the specified DOM
     * Returns the root element on the destination DOM.
     * @param type $dom
     */
    public function export(\DOMDocument $dom){
        $this->assembleNodes();
        return $dom->importNode($this->book, TRUE);
    }

    public function setFeatured($value) {
        $this->featured = $this->dom->createAttribute(self::ATTR_FEATURED);
        $this->featured->value = $value == true? "true" : "false";
    }

    public function setTitle($content) {
        $this->title = $this->dom->createElement(self::TAG_TITLE, $content);
    }

    public function setAuthor($content) {
        $this->author = $this->dom->createElement(self::TAG_AUTHOR, $content);
    }

    public function setIsbn($content) {
        $this->isbn = $this->dom->createElement(self::TAG_ISBN, $content);
    }

    public function setPublisher($content) {
        $this->publisher = $this->dom->createElement(self::TAG_PUBLISHER, $content);
    }
    
    public function setYear($content) {
        $this->year = $this->dom->createElement(self::TAG_YEAR, $content);
    }

    public function addCategory($content) {
        if (!in_array($content, $this->categoriesArray)) {
            $this->categoriesArray[] = $content;
        }
    }
    
    public function setDescription($content){
        $this->description = $this->dom->createElement(self::TAG_DESCRIPTION);
        $this->description->appendChild($this->dom->createTextNode($content));
    }
    
    public function setAvgRating($content){
        $this->avgRating = $this->dom->createElement(self::TAG_AVG_RATE, $content);
    }

    public function setImage($content){
        $this->image = $this->dom->createElement(self::TAG_IMAGE);
        $this->image->appendChild($this->dom->createTextNode($content));
    }
    
    public function setPreviewLink($content){
        $this->previewLink = $this->dom->createElement(self::TAG_PREV_LINK);
        $this->previewLink->appendChild($this->dom->createTextNode($content));
    }
    
    public function setInfoLink($content){
        $this->infoLink = $this->dom->createElement(self::TAG_INFO_LINK);
        $this->infoLink->appendChild($this->dom->createTextNode($content));
    }
    
    public function setCategories($categories) {
        $this->categoriesArray = $categories;
        $this->categories = $this->dom->createElement(self::TAG_CATEGORIES);
        foreach ($categories as $categoryValue) {
            $element = $this->dom->createElement(self::TAG_CATEGORY, $categoryValue);
            $this->categories->appendChild($element);
        }
    }

    private function assembleNodes() {
        if(isset($this->featured)){
            $this->book->setAttributeNode($this->featured);
        }
        $this->dom->appendChild($this->book);
        if (isset($this->title)) {
            $this->book->appendChild($this->title);
        }
        if (isset($this->author)) {
            $this->book->appendChild($this->author);
        }
        if (isset($this->isbn)) {
            $this->book->appendChild($this->isbn);
        }
        if (isset($this->publisher)) {
            $this->book->appendChild($this->publisher);
        }
        if (isset($this->year)) {
            $this->book->appendChild($this->year);
        }
        if (isset($this->description)) {
            $this->book->appendChild($this->description);
        }
        if (isset($this->avgRating)) {
            $this->book->appendChild($this->avgRating);
        }if (isset($this->image)) {
            $this->book->appendChild($this->image);
        }
        if (isset($this->previewLink)) {
            $this->book->appendChild($this->previewLink);
        }
        if (isset($this->infoLink)) {
            $this->book->appendChild($this->infoLink);
        }
        
        if (!empty($this->categoriesArray)) {
            $this->setCategories($this->categoriesArray);
            $this->book->appendChild($this->categories);
        }
    }

}
