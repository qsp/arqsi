<?php
/**
 * ARQSI:
 * Bruno Flávio - 1040865 || Luís Teixeira - 1050510
 */

require_once 'ListDOM.php';

/**
 * BookDOM
 * Classe que é responsável para  construção de um documento DOM que contém um
 * livro e os seus atributos.
 *
 */
class BooksDOM extends ListDOM {

    const TAG_ROOT = 'books';
    const TAG_CATEGORY = 'book';

    private $isbnArray = array();

    public function __construct() {
        parent::__construct(self::TAG_ROOT, self::TAG_CATEGORY);
    }

    //recebe um bookDOM por parametro,
    //verifica se o mesmo livro (isbn) já foi inserido préviamente.
    public function addElement($element) {
        //extract the book:
        $book = $element->firstChild;
        $isbnList = $element->getElementsByTagName(BookDOM::TAG_ISBN);
        $isbn = "";
        if (!empty($isbnList)) {
            $isbn = $isbnList->item(0)->nodeValue;
        }
        
        if (!in_array($isbn, $this->isbnArray, TRUE)) {
            $this->isbnArray[] = $isbn;            
            parent::addElement($element);
            return true;
        } else {
            file_put_contents(
                    'php://stderr',
                    "ISBN duplicado: $isbn;\n");
            return false;
        }
    }

    protected function writeValueToElement($value, DOMElement $element) {
        foreach ($value->childNodes as $child) {
            $localChild = parent::import($child);
            $element->appendChild($localChild);
        }
        foreach ($value->attributes as $attribute) {
            $element->setAttribute($attribute->name, $attribute->value);
        }
    }

}
