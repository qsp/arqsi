<?php
/**
 * ARQSI:
 * Bruno Flávio - 1040865 || Luís Teixeira - 1050510
 */

require_once 'ListDOM.php';

/**
 * BookDOM
 * Classe que é responsável para  construção de um documento DOM que contém um
 * livro e os seus atributos.
 *
 */
class CategoriesDOM extends ListDOM{

    const TAG_ROOT = 'categories';
    const TAG_CATEGORY = 'category';
    
    private $elementsArray = array();
    
    public function __construct() {
        parent::__construct(self::TAG_ROOT, self::TAG_CATEGORY);
    }

    public function addElement($element) {
        
        if (!in_array($element, $this->elementsArray)) {
            $elementsArray[] = $element;            
            parent::addElement($element);
        } else {
            file_put_contents('php://stderr', 'CATEGORIA duplicada: ' . print_r($element, TRUE) . "\n");
        }
    }
}
