<?php
/**
 * ARQSI:
 * Bruno Flávio - 1040865 || Luís Teixeira - 1050510
 */

require_once './model/BooksDOM.php';
class GetBookDetailsService {

    private $publishersAdapters;
    private $isbn;
    private $publisherName;

    public function __construct($publishersAdapters, $isbn, $publisher) {
        $this->publishersAdapters = $publishersAdapters;
        $this->isbn = $isbn;
        $this->publisherName = $publisher; 
    }
    
    public function run(){
        $publisherAdapter = null;
        foreach($this->publishersAdapters as $adapter){
            if($adapter->getName() == $this->publisherName){
                $publisherAdapter = $adapter;
            }
        }
        $bookDetails = null;
        if(isset($publisherAdapter)){
            $bookDetails = $publisherAdapter->getBookDetails($this->isbn);
        }
        return $bookDetails->getDOM()->saveXML();
    }
}
