<?php
/**
 * ARQSI:
 * Bruno Flávio - 1040865 || Luís Teixeira - 1050510
 */

require_once './model/BooksDOM.php';

class FindBooksByNameService {
    private $publishersAdapters;
    private $name;

    public function __construct($publishersAdapters, $name) {
        $this->publishersAdapters = $publishersAdapters;
        $this->name = $name;
    }
    
    public function run(){
        $booksDOM = new BooksDOM();
        foreach($this->publishersAdapters as $adapter){
            $responseDOM = $adapter->findBooksByName($this->name);
            for($i = 0; $i < $responseDOM->length(); $i++){
                $booksDOM -> addElement($responseDOM->getElement($i));
            }
        }
        return $booksDOM->getDOM()->saveXML();
    }
}
