<?php
/**
 * ARQSI:
 * Bruno Flávio - 1040865 || Luís Teixeira - 1050510
 */

/**
 * Description of GetCategoriesService
 *
 */
require_once './model/CategoriesDOM.php';

class GetCategoriesService {
    private $publishersAdapters;

    public function __construct($publishersAdapters) {
        $this->publishersAdapters = $publishersAdapters;
    }
    
    public function run(){
        $categoriesDOM = new CategoriesDOM();
        foreach($this->publishersAdapters as $adapter){
            $responseDOM = $adapter->listAllCategories();
            for($i = 0; $i < $responseDOM->length(); $i++){
                $categoriesDOM->addElement($responseDOM->getElement($i));
            }
        }
        return $categoriesDOM->getDOM()->saveXML();
    }
}
