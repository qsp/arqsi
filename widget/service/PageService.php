<?php
/**
 * ARQSI:
 * Bruno Flávio - 1040865 || Luís Teixeira - 1050510
 */

require_once './model/BooksDOM.php';

class PageService {
    private $previousData;
    private $pageOptions;

    public function __construct($previousData, $pageOptions) {
        $this->previousData = $previousData;
        $this->pageOptions  = $pageOptions;
    }

    public function run() {
        $first = $this->pageOptions["first"];
        $items = $this->pageOptions["items"];
        $total = $this->previousData->count();
        
        $page = new BooksDOM();
        for ($pos = $first; $pos < $first + $items && $pos < $total; $pos++) {
            $page->addElement($this->previousData->getElement($pos));
        }
        
        return $page->getDOM()->saveXML();
    }

}
