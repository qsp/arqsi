<?php
/**
 * ARQSI:
 * Bruno Flávio - 1040865 || Luís Teixeira - 1050510
 */

require_once "GoogleBookHelper.php";
require_once "ExternalRequest/ExternalRequestFactory.php";
require_once "model/BookDOM.php";
require_once "model/BooksDOM.php";
require_once "model/CategoriesDOM.php";

/**
 * Concretization of the PublishedAdaptor for the publishing webservice 
 * provided by phpdev2.dei.isep.ipp.pt/
 *
 */
class PublisherPhpdev2Adapter implements PublisherAdapter {
    /* API: parametros permitidos: */

//    private static $api_commands = array(
//        "editora" => "",
//        "titulo" => "pesquisa",
//        "numero" => "book",
//        "news" => "book",
//        "categoria" => "book",
//    );
    //MAPPING PARAMETERS:    
    private static $apiMap = array(
        "title" => "title",
        "author" => "author",
        "isbn" => "isbn",
        "category" => "category",
        "year" => "publicacao",
        "featured" => "news",
    );
    private $url;
    private $name;

    public function findBooksByName($name) {
        //$dom = new DOMDocument('1.0', 'UTF-8');
        $booksFound = new BooksDOM();
        
        $publisherBooks = $this->requestBookByName($name);        

        foreach ($publisherBooks as $incommingBook) {            

            //Reading incomming book parameters:
            $parameters = array();
            foreach ($incommingBook->childNodes as $parameter) {
                $parameters[$parameter->nodeName] = $parameter;                
            }                        
            
            //Create the book:
            $book = new BookDOM();
            $book->setFeatured($parameters[ "news"]->nodeValue === "sim" ?
                                            true : false
                              );            
            
            $book->setTitle($parameters["title"]->nodeValue);
            $book->setAuthor($parameters["author"]->nodeValue);
            $book->setIsbn($parameters["isbn"]->nodeValue);           
            $book->addCategory($parameters["category"]->nodeValue);
            $book->setPublisher($this->name);
            $book->setYear($parameters["publicacao"]->nodeValue);
                        
            $booksFound->addElement($book->getDOM()->firstChild);
        }
        
        return $booksFound;
    }

    public function getBookDetails($isbn) {
        return GoogleBookHelper::getBookDetails($isbn, $this->name);
    }

    public function getName() {
        return $this->name;
    }

    public function listAllCategories() {
        $publisherCategories = $this->requestAllCategories();
        $categoriesFound = new CategoriesDOM();
        
        foreach($publisherCategories as $publisherCategory) {
            $categoriesFound->addElement($publisherCategory->nodeValue);
        }
        
        return $categoriesFound;
    }

    public function listBooksByCategory($category, $max=null) {
        
        $booksFound = new BooksDOM();        
        /**
         * Desativei a utilização do parametro numero na api do phpdev2
         * visto que não se comporta como o esperado (ignora a categoria).
         */
        $publisherBooks = $this->requestBooksByCategory($category);        

        foreach ($publisherBooks as $incommingBook) {            

            //Reading incomming book parameters:
            $parameters = array();
            foreach ($incommingBook->childNodes as $parameter) {
                $parameters[$parameter->nodeName] = $parameter;                
            }                        
            
            //Create the book:
            $book = new BookDOM();
            $book->setFeatured($parameters[ "news"]->nodeValue === "sim" ?
                                            true : false
                              );            
            
            $book->setTitle($parameters["title"]->nodeValue);
            $book->setAuthor($parameters["author"]->nodeValue);
            $book->setIsbn($parameters["isbn"]->nodeValue);           
            $book->addCategory($parameters["category"]->nodeValue);
            $book->setPublisher($this->name);
            $book->setYear($parameters["publicacao"]->nodeValue);
                        
            $booksFound->addElement($book->getDOM()->firstChild);
            
            if (isset($max)) {
                $max--;
                if ($max === 0) break;
            }
        }
        return $booksFound;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setURL($url) {
        $this->url = $url;
    }

    private function requestBookByName($name) {
        $query = array(
            "titulo" => $name
        );

        $request = ExternalRequestFactory::build(ConcreteExternalRequest::XML, $this->url);
        $request->setParameters($query);
        $request->request();
        $reply = $request->getData("books");
        
        return $reply->getElementsByTagName("book");
    }
    private function requestBooksByCategory($category, $max=null) {
        $query = array(
            "categoria" => $category
        );

        if(isset($max)) {
            $query['numero'] = $max;
        }
        
        $request = ExternalRequestFactory::build(ConcreteExternalRequest::XML, $this->url);
        $request->setParameters($query);
        $request->request();
        $reply = $request->getData("books");
        
        return $reply->getElementsByTagName("book");
    }

    private function requestAllCategories() {
        $query = array(
            "categoria" => "todas"
        );

        $request = ExternalRequestFactory::build(ConcreteExternalRequest::XML, $this->url);
        $request->setParameters($query);
        $request->request();
        $reply = $request->getData();
        
        return $reply->getElementsByTagName('categoria');
    }
}
