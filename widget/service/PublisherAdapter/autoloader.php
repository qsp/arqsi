<?php
/**
 * ARQSI:
 * Bruno Flávio - 1040865 || Luís Teixeira - 1050510
 */

//Adapters autoloader:
function publisherLoader($class) {
    if (strpos($class,'Publisher') !== false) {
        require_once $class . '.php';
    }
}

spl_autoload_register('publisherLoader');
