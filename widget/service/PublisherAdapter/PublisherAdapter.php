<?php
/**
 * ARQSI:
 * Bruno Flávio - 1040865 || Luís Teixeira - 1050510
 */

/**
 * PublisherAdapter.php
 * 
 * This interface contains the necessary interface to communicate with
 * publisher companies API's.
 * 
 * @author Luís Teixeira    - 1050510
 * @author Bruno Flávio     - 1040865
 */


interface PublisherAdapter {
    public function setName($name);
    public function setURL($url);
    public function getName();
    public function listAllCategories();
    public function listBooksByCategory($category, $max);
    public function findBooksByName($name);
    public function getBookDetails($isbn);
}
