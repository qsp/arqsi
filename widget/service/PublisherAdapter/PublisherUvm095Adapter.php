<?php
/**
 * ARQSI:
 * Bruno Flávio - 1040865 || Luís Teixeira - 1050510
 */

require_once "GoogleBookHelper.php";
require_once "ExternalRequest/ExternalRequestFactory.php";
require_once "model/BookDOM.php";
require_once "model/BooksDOM.php";
require_once "model/CategoriesDOM.php";

/**
 * Concretization of the PublishedAdaptor for the publishing webservice 
 * portoeditora http://uvm095.dei.isep.ipp.pt/portoeditora/api.php
 *
 */
class PublisherUvm095Adapter implements PublisherAdapter {

    //MAPPING PARAMETERS:
    private static $apiMap = array(
        "title" => "title",
        "author" => "author",
        "isbn" => "isbn",
        "category" => "category",
        "year" => "edition",
        "featured" => "featured",
    );
    private $url;
    private $name;

    public function findBooksByName($name) {

        $booksFound = new BooksDOM();
        
        $publisherBooks = $this->requestBookByName($name);        

        foreach ($publisherBooks as $parameters) {
            //Create the book:
            $book = new BookDOM();
            $book->setFeatured($parameters[ "featured"] === "yes" ?
                                            true : false
                              );
            $book->setTitle($parameters["title"]);
            $book->setAuthor($parameters["author"]);
            $book->setIsbn($parameters["isbn"]);           
            $book->addCategory($parameters["category"]);
            $book->setPublisher($this->name);
            $book->setYear($parameters["edition"]);
                        
            $booksFound->addElement($book->getDOM()->firstChild);
        }
        
        return $booksFound;
    }

    public function getBookDetails($isbn) {        
        return GoogleBookHelper::getBookDetails($isbn, $this->name);
    }
    
    public function getName() {
        return $this->name;
    }

    public function listAllCategories() {
        $publisherCategories = $this->requestAllCategories();
        $categoriesFound = new CategoriesDOM();
        
        foreach($publisherCategories as $publisherCategory) {
            $categoriesFound->addElement($publisherCategory);
        }
        
        return $categoriesFound;
    }

    public function listBooksByCategory($category, $max=null) {
        
        $booksFound = new BooksDOM();        
        $publisherBooks = $this->requestBooksByCategory($category, $max);        

        foreach ($publisherBooks as $parameters) {
            //Create the book:
            $book = new BookDOM();
            $book->setFeatured($parameters[ "featured"] === "yes" ?
                                            true : false
                              );
            $book->setTitle($parameters["title"]);
            $book->setAuthor($parameters["author"]);
            $book->setIsbn($parameters["isbn"]);           
            $book->addCategory($parameters["category"]);
            $book->setPublisher($this->name);
            $book->setYear($parameters["edition"]);
                        
            $booksFound->addElement($book->getDOM()->firstChild);
            
            if (isset($max)) {
                $max--;
                if ($max === 0) break;
            }
        }
        return $booksFound;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setURL($url) {
        $this->url = $url;
    }        

    private function requestBookByName($name) {
        $query = array(
            "title" => $name
        );

        $request = ExternalRequestFactory::build(ConcreteExternalRequest::JSON, $this->url);
        $request->setParameters($query);
        $request->request();
        $reply = $request->getData();
        
        return json_decode($reply, true);
    }
    private function requestBooksByCategory($category, $max=null) {
        $query = array(
            "category" => $category
        );

        $request = ExternalRequestFactory::build(ConcreteExternalRequest::JSON, $this->url);
        $request->setParameters($query);
        $request->request();
        $reply = $request->getData();
        
        return json_decode($reply, true);
    }

    private function requestAllCategories() {
        $query = array(
            "category" => "all"
        );

        $request = ExternalRequestFactory::build(ConcreteExternalRequest::JSON, $this->url);
        $request->setParameters($query);
        $request->request();
        $reply = $request->getData();
        
        return json_decode($reply, true);
    }
}
