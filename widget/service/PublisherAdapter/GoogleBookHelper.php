<?php

/**
 * ARQSI:
 * Bruno Flávio - 1040865 || Luís Teixeira - 1050510
 */

class GoogleBookHelper {

    private static $defaultDescription = "No Decription Available";
    private static $defaultImage = "/img/Cover-not-available.jpg";
    private static $googleBooksUrl = "https://www.googleapis.com/books/v1/volumes";
    private static $googleAPIKey = "AIzaSyBguRqUaGDZ5FKBiZvmZVhcojwN_xux1Cw";

    public static function getBookDetails($isbn, $publisher) {
        $book = new BookDOM();
        /* Fields that we will extract if present
         * description => response["items"]["volumeInfo"]["description"]
         * averageRating => response["items"]["volumeInfo"]["averageRating"]
         * image => response["items"]["volumeInfo"]["imageLinks"]["thumbnail"]
         * previewLink => response["items"]["volumeInfo"]["previewLink"]
         * infoLink => esponse["items"]["volumeInfo"]["infoLink"]        
         */
        $bookDetails = self::requestBookDetails($isbn);
        $response = json_decode($bookDetails, TRUE);
        $description = $response["items"][0]["volumeInfo"]["description"];
        $averageRating = $response["items"][0]["volumeInfo"]["averageRating"];
        $image = $response["items"][0]["volumeInfo"]["imageLinks"]["thumbnail"];
        $previewLink = $response["items"][0]["volumeInfo"]["previewLink"];
        $infoLink = $response["items"][0]["volumeInfo"]["infoLink"];

        $book->setIsbn($isbn);
        $book->setpublisher($publisher);
        if (isset($description)) {
            $book->setDescription($description);
        } else {
            $book->setDescription(self::$defaultDescription);
        }
        if (isset($averageRating)) {
            $book->setAvgRating($averageRating);
        }
        if (isset($image)) {
            $book->setImage($image);
        } else {
            $book->setImage(self::$defaultImage);
        }
        if (isset($previewLink)) {
            $book->setPreviewLink($previewLink);
        }
        if (isset($infoLink)) {
            $book->setInfoLink($infoLink);
        }

        return $book;
    }

    private static function requestBookDetails($isbn) {
        $query = array(
            "q" => "isbn:" . $isbn,
            "key" => self::$googleAPIKey
        );
        $request = ExternalRequestFactory::build(ConcreteExternalRequest::JSON, self::$googleBooksUrl);
        $request->setParameters($query);
        $request->request();
        return $request->getData();
    }

}
