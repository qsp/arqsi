<?php
/**
 * ARQSI:
 * Bruno Flávio - 1040865 || Luís Teixeira - 1050510
 */



class PublisherAdapterFactory {
    
    public function createPublisherAdapter($name, $url, $classPath){
       require_once 'PublisherAdapter/' . $classPath . '.php';
       $publisherAdapter = new $classPath;
       $publisherAdapter->setName($name);
       $publisherAdapter->setUrl($url);
       
       return $publisherAdapter;
    }
}
