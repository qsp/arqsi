<?php

$client = new SoapClient ('http://wvm095.dei.isep.ipp.pt/Service1.svc?wsdl'); 
$result = $client->getProductsCatalog();

$json = json_decode($result->getProductsCatalogResult,TRUE);

$objselected = rand(0, count($json) - 1);

$discount =(rand(1, 20) / 100);

$id = intval($json[$objselected]['id']);

$params = array("id" => $id, "discount" => $discount);

$client->setDiscount($params);

