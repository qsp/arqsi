<?php
    
    require_once 'nusoap.php';
    
    $server = new soap_server();
    $server->soap_defencoding = 'UTF-8'; 
    
    $server -> configureWSDL("shippingallservice", "urn:shippingallservice");
    
    $server -> register(
            "GetShippingRate",
            array("NumberOfItems" => "xsd:int"),
            array("GetShippingRateResult" => "xsd:float"),
            "urn:shippingallservice",
            "urn:shippingallservice#GetShippingRate",
            "rpc"
            );

    function GetShippingRate($NumberOfItems){
        $RatePerItem = 1.5;
        
        return $NumberOfItems * $RatePerItem;
    }
    
    $HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
    $server->service($HTTP_RAW_POST_DATA);
?>
