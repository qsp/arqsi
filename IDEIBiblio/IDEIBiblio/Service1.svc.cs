﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using IDEIBiblio.Models;
using System.Text;
using System.Web.Script.Serialization;

namespace IDEIBiblio
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        public struct WSProduct
        {
            public int id;
            public string name;
            public decimal price;
        }

        private WSProduct convertProduct(Product p)
        {
            WSProduct wsp;
            wsp.id = p.ProductID;
            wsp.price = p.CurrentPrice;
            wsp.name = p.Name;

            return wsp;
        }

        private bool checkApiKey(string key)
        {
            //guardar na db se colocado em produção.
            string apikey = "1234";

            return key.Equals(apikey);
        }

        public string getProductsCatalog(/*string apikey*/)
        {
            //if (!checkApiKey(apikey)) return "";

            Models.IDEIBiblioDbContext context = new Models.IDEIBiblioDbContext();
            List<Product> allProducts = context.Products.ToList();

            List<WSProduct> allWSProducts = new List<WSProduct>();
            foreach (Product p in allProducts)
            {                
                allWSProducts.Add(convertProduct(p));
            }

            var jsonSerialiser = new JavaScriptSerializer();
            var jsonList = jsonSerialiser.Serialize(allWSProducts);


            return  jsonList;
        }

        public void setDiscount(/*string apikey,*/ int id, float discount)
        {
            //if (!checkApiKey(apikey)) return "";

            //só permitimos desconto máximo de 20%
            decimal maxDiscount = 0.20M;
            decimal newDiscount = ((decimal)discount < maxDiscount)? (decimal)discount : maxDiscount;

            Models.IDEIBiblioDbContext context = new Models.IDEIBiblioDbContext();
            Product Product = context.Products.Find(id);

            if (Product == null) return;
            Product.DiscountPercentage = newDiscount;

            context.SaveChanges();
        }
    }
}
