﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

using IDEIBiblio.Models;

namespace IDEIBiblio.AuthorizeAttributes
{
    public class AuthorizeRole : AuthorizeAttribute
    {
        public string AccessLevel { get; set; }



        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (!httpContext.Request.IsAuthenticated)
                return false;

            HttpRequestBase Request = httpContext.Request;
            string Controller = Request.RequestContext.RouteData.Values["controller"].ToString();
            string Action = Request.RequestContext.RouteData.Values["action"].ToString();
            string UserName = httpContext.User.Identity.Name;

            IDEIBiblioDbContext db = new IDEIBiblioDbContext();
            ICollection<Role> Roles = db.GetRolesForUser(UserName);
            ICollection<Activity> RolesActivities = GetRolesActivities(Roles);

            foreach (Activity Activity in RolesActivities)
            {
                if (Activity.Controller.ToUpper() == Controller.ToUpper())
                {
                    if (Activity.Action.ToUpper() == Action.ToUpper())
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private ICollection<Activity> GetRolesActivities(ICollection<Role> Roles)
        {
            IDEIBiblioDbContext db = new IDEIBiblioDbContext();
            LinkedList<Activity> AuthorizedActivities = new LinkedList<Activity>();
            foreach (Role Role in Roles)
            {
                if (Role == null || Role.AuthorizedActions == null)
                {
                    continue;
                }
                foreach (RoleActivity RoleActivity in Role.AuthorizedActions)
                {
                    Activity Activity = RoleActivity.Activity;
                    if (!AuthorizedActivities.Contains(Activity))
                    {
                        AuthorizedActivities.AddLast(Activity);
                    }
                }
            }
            return AuthorizedActivities;
        }
    }
}