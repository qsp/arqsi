﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IDEIBiblio.Models
{
    public class UserInRole
    {
        public int UserInRoleID { get; set; }

        public int UserID { get; set; }
        public int RoleID { get; set; }

        public virtual UserProfile User { get; set; }
        public virtual Role Role { get; set; }

        public UserInRole() { }

        public UserInRole(UserProfile User, Role Role)
        {
            this.User = User;
            this.Role = Role;
        }
    }
}