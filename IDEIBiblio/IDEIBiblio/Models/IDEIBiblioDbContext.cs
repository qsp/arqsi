﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace IDEIBiblio.Models
{
    public class IDEIBiblioDbContext : DbContext
    {
        public IDEIBiblioDbContext() : base("DefaultConnection") { }

        public DbSet<Product> Products { get; set; }
        public DbSet<Magazine> Magazines { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<BookCategory> BookCategories { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<Publisher> Publishers { get; set; }

        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderLine> OrderLines { get; set; }
        public DbSet<Delivery> Deliveries { get; set; }
        public DbSet<LogisticsCompany> LogisticsCompanies { get; set; }

        public DbSet<ShoppingCart> ShoppingCarts { get; set; }
        public DbSet<ShoppingCartLine> ShoppingCartLines { get; set; }

        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Activity> Activities { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<RoleActivity> RoleActions { get; set; }
        public DbSet<UserInRole> UserInRole { get; set; }

        public void AddUserToRole(string UserName, string RoleName)
        {
            UserProfile User = this.UserProfiles.SingleOrDefault(user => user.UserName == UserName);
            Role Role = this.Roles.SingleOrDefault(role => role.RoleName == RoleName);

            if (User != null && Role != null)
            {
                UserInRole UserInRole = new UserInRole(User, Role);
                User.UserRoles.Add(UserInRole);
                this.SaveChanges();
            }
        }

        public void RemoveUserFromRole(int UserId, int RoleId)
        {
            UserProfile User = this.UserProfiles.Find(UserId);
            Role Role = this.Roles.Find(RoleId);

            if (User != null && Role != null)
            {
                UserInRole UserInRole = this.UserInRole.Single(uir => uir.RoleID == RoleId && uir.UserID == UserId);
                User.UserRoles.Remove(UserInRole);
                this.UserInRole.Remove(UserInRole);
                this.SaveChanges();
            }
        }

        public ICollection<Role> GetRolesForUser(string UserName)
        {
            UserProfile User = this.UserProfiles.SingleOrDefault(user => user.UserName == UserName);
            LinkedList<Role> Roles = new LinkedList<Role>();
            if (User != null)
            {
                foreach (UserInRole UserInRole in User.UserRoles)
                {
                    Roles.AddLast(UserInRole.Role);
                }
            }
            return Roles;
        }

        public void AddActivityToRole(string ControllerName, string ActionName, string RoleName)
        {
            Role Role = this.Roles.SingleOrDefault(role => role.RoleName == RoleName);
            Activity Activity = this.Activities.FirstOrDefault(activity => activity.Controller == ControllerName && activity.Action == ActionName);

            if (Role != null && Activity != null)
            {
                RoleActivity RoleActivity = new RoleActivity
                {
                    Role = Role,
                    Activity = Activity
                };
                Role.AuthorizedActions.Add(RoleActivity);
                this.SaveChanges();
            }
        }

        public void RemoveActivityFromRole(int ActivityId, int RoleId)
        {
            Activity Activity = this.Activities.Find(ActivityId);
            Role Role = this.Roles.Find(RoleId);

            if (Activity != null && Role != null)
            {
                RoleActivity RoleActivity = this.RoleActions.Single(uir => uir.RoleID == RoleId && uir.ActivityID == ActivityId);
                Role.AuthorizedActions.Remove(RoleActivity);
                this.RoleActions.Remove(RoleActivity);
                this.SaveChanges();
            }
        }

        public ShoppingCart GetShoppingCart(string SessionID, string UserName)
        {
            if (UserName == null)
            {
                return GetAnonymousUserShoppingCart(SessionID);
            }
            else
            {
                return GetUserShoppingCart(UserName);
            }
        }

        public void TransferCartToUser(string SessionID, string UserName)
        {
            UserProfile User = UserProfiles.SingleOrDefault(shoppingcart => shoppingcart.UserName == UserName);
            ShoppingCart cart = ShoppingCarts.SingleOrDefault(shoppingcart => shoppingcart.SessionID == SessionID);
            cart.SessionID = "";
            cart.UserID = User.UserId;
            SaveChanges();
        }

        public ICollection<Book> GetMostOrderedBooks()
        {
            List<Book> TopOrderedBooks = new List<Book>();

            var TopBooksIds = OrderLines.Join(Books, line => line.ProductID, book => book.ProductID, (line, book) => new { Id = line.ProductID }).OrderByDescending(res => res.Id).Select(g => new { ID = g.Id }).Distinct().Take(5);

            foreach (var item in TopBooksIds)
            {
                TopOrderedBooks.Add(Books.Find(item.ID));
            }

            return TopOrderedBooks;
        }

        public ICollection<Magazine> GetMostOrderedMagazines()
        {
            List<Magazine> TopOrderedMagazines = new List<Magazine>();

            var TopMagazinesIds = OrderLines.Join(Magazines, line => line.ProductID, mag => mag.ProductID, (line, mag) => new { Id = line.ProductID }).OrderByDescending(res => res.Id).Select(g => new { ID = g.Id }).Distinct().Take(5);

            foreach (var item in TopMagazinesIds)
            {
                TopOrderedMagazines.Add(Magazines.Find(item.ID));
            }

            return TopOrderedMagazines;

        }

        /* Helper methods*/
        private ShoppingCart GetAnonymousUserShoppingCart(string SessionID)
        {
            ShoppingCart cart = ShoppingCarts.SingleOrDefault(shoppingcart => shoppingcart.SessionID == SessionID);
            if (cart == null)
            {
                cart = new ShoppingCart();
                cart.SessionID = SessionID;
                ShoppingCarts.Add(cart);
            }
            else
            {
                cart.SessionID = SessionID; 
            }
            SaveChanges();
            return cart;
        }

        private ShoppingCart GetUserShoppingCart(string UserName)
        {
            UserProfile User = UserProfiles.SingleOrDefault(user => user.UserName == UserName);
            ShoppingCart cart = ShoppingCarts.SingleOrDefault(shoppingcart => shoppingcart.UserID == User.UserId);
            SaveChanges();
            return cart;
        }
    }
}