﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace IDEIBiblio.Models
{
    [Table("Roles")]
    public class Role
    {
        public int RoleID { get; set; }
        public string RoleName { get; set; }

        public virtual ICollection<RoleActivity> AuthorizedActions { get; set; }
    }
}