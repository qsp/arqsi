﻿using IDEIBiblio.ServiceClient.ShippingServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace IDEIBiblio.Models
{
    public class Order
    {
        public int OrderID { get; set; }
        public int UserID { get; set; }

        public DateTime CreationDate { get; set; }
        [DataType(DataType.MultilineText)]
        public string Address { get; set; }
        public int? DeliveryID { get; set; }
        
        public virtual ICollection<OrderLine> OrderLines { get; set; }
        public virtual UserProfile User { get; set; }
        public virtual Delivery Delivery { get; set; }

        public Order(){
            this.CreationDate = System.DateTime.Now;
            this.OrderLines = new List<OrderLine>();
        }

        public void TransferCart(ShoppingCart Cart)
        {
            ICollection<OrderLine> OrderLines = new List<OrderLine>();
            foreach (ShoppingCartLine CartLine in Cart.ShoppingCartLines)
            {
                OrderLine OrderLine = CreateOrderLine(CartLine);
                OrderLines.Add(OrderLine);
            }
            Cart.ClearCart();
            this.OrderLines = OrderLines;
        }

        private OrderLine CreateOrderLine(ShoppingCartLine CartLine)
        {
            OrderLine OrderLine = new OrderLine
            {
                Order = this,
                ProductID = (int)CartLine.ProductID,
                Quantity = CartLine.Quantity,
                UnitaryPrice = CartLine.Product.DiscountedPrice()
            };
            return OrderLine;
        }
    }

    public class OrderLine
    {
        public int OrderLineID { get; set; }

        public int OrderID { get; set; }
        public int ProductID { get; set; }

        public virtual Order Order { get; set; }
        public virtual Product Product { get; set; }
        
        public int Quantity { get; set; }
        [DataType(DataType.Currency)]
        public decimal UnitaryPrice { get; set; }        
    }

    public class Delivery
    {
        public int DeliveryID { get; set; }

        public int LogisticsCompanyID { get; set; }        
        public string TrackingNo { get; set; }
        public decimal Price { get; set; }

        public DateTime? EstimatedDeliveryDate { get; set; }
    }

    public class LogisticsCompany
    {
        public int LogisticsCompanyID { get; set; }

        public string CompanyName { get; set; }
        public string ServiceName { get; set; }
        public string WebServiceURL { get; set; }
        public string LogisticsAdapterClass { get; set; }

        public decimal GetShippingRate(int NumberOfItems)
        {
            IShippingServiceAdapter Adapter = ShippingServiceAdapterFactory.CreateShippingServiceAdapter(this.LogisticsAdapterClass, this.WebServiceURL, this.ServiceName);

            return Adapter.GetShippingRate(NumberOfItems);
        }
    }
}