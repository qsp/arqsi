﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IDEIBiblio.Models
{
    public class ShoppingCart
    {
        public int ShoppingCartID { get; set; }
        
        public int? UserID { get; set; }
        public string SessionID { get; set; }

        public DateTime? LastUpdate { get; set; }

        public virtual ICollection<ShoppingCartLine> ShoppingCartLines { get; set; }
        public virtual UserProfile User { get; set; }

        /* Instance Methods*/
        public int GetTotalNumberOfProducts()
        {
            int Total = 0;
            if (ShoppingCartLines != null)
            {
                foreach (ShoppingCartLine line in ShoppingCartLines)
                {
                    Total += line.Quantity;
                }
            }
            return Total;
        }

        public decimal PriceTotal()
        {
            decimal Total = 0M;
            if (ShoppingCartLines != null)
            {
                foreach (ShoppingCartLine line in ShoppingCartLines)
                {
                    Total += line.PriceTotal();
                }
            }
            return Total;
        }

        public void AddProductToCart(Product Product)
        {
            ShoppingCartLine Line = this.ShoppingCartLines.FirstOrDefault(cardline => cardline.ProductID == Product.ProductID);
            if (Line == null)
            {
                Line = new ShoppingCartLine
                {
                    ShoppingCart = this,
                    Product = Product,
                    Quantity = 1
                };
                this.ShoppingCartLines.Add(Line);
            }
            else
            {
                Line.Quantity++;
            }
        }

        public void RemoverFromCart(ShoppingCartLine Line)
        {
            this.ShoppingCartLines.Remove(Line);
        }

        public void ClearCart()
        {
            ICollection<ShoppingCartLine> LinesToRemove = new List<ShoppingCartLine>(this.ShoppingCartLines);
            foreach(ShoppingCartLine Line in LinesToRemove)
            {
                RemoverFromCart(Line);
            }
        }

    }

    public class ShoppingCartLine
    {
        public int ShoppingCartLineID { get; set; }

        public int? ShoppingCartID { get; set; }
        public int? ProductID { get; set; }

        public virtual ShoppingCart ShoppingCart { get; set; }
        public virtual Product Product { get; set; }

        public int Quantity { get; set; }

        public decimal PriceTotal()
        {
            return Product.DiscountedPrice() * Quantity;
        }
    }

}