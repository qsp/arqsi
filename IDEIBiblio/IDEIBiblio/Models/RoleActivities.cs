﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.ComponentModel.DataAnnotations.Schema;

namespace IDEIBiblio.Models
{
    [Table("RoleActivities")]
    public class RoleActivity
    {
        public int RoleActivityID{ get; set; }

        public int RoleID { get; set; }
        public int ActivityID { get; set; }

        public virtual Role Role { get; set; }
        public virtual Activity Activity { get; set; }
    }
}