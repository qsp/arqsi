﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IDEIBiblio.Models
{

    public class Product
    {
        
        public int ProductID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        [DataType(DataType.ImageUrl)]
        public string ImageURL { get; set; }

        [DataType(DataType.Currency)]
        public decimal CurrentPrice { get; set; }

        [Display(Name = "Current Discount")]
        public decimal DiscountPercentage { get; set; }

        public decimal DiscountedPrice()
        {
            decimal discountedAmmount = (CurrentPrice * DiscountPercentage);
            return Math.Round(CurrentPrice - discountedAmmount, 2);
        }
    }


    public class Magazine : Product
    {
        public int EditionNumber { get; set; }
        
        [Display(Name = "Publishing Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime PublishingDate { get; set; }
    }


    public class Book : Product
    {

        [Required]
        [Display(Name = "ISBN")]
        public string Isbn { get; set; }

        [Display(Name = "Publishing Date")]
        [DisplayFormat(ApplyFormatInEditMode = true,DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime PublishingDate { get; set; }        

        public int AuthorID { get; set; }
        public virtual Author Author { get; set; }

        public int PublisherID { get; set; }
        public virtual Publisher Publisher { get; set; }

        public int BookCategoryID { get; set; }
        public virtual BookCategory BookCategory { get; set; }
        
    }


    public class BookCategory
    {

        public int BookCategoryID { get; set; }

        [Display(Name="Category")]
        public string Name { get; set; }

        public virtual ICollection<Book> books { get; set; }
    }


    public class Author
    {
        
        public int AuthorID { get; set; }

        public string Name { get; set; }

        public virtual ICollection<Book> books { get; set; }
    }


    public class Publisher
    {
                
        //[Required, Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PublisherID { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Book> books { get; set; }
    }

}