﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;


namespace IDEIBiblio.Models
{
    [Table("Activities")]
    public class Activity
    {
        [Key]
        public int ID { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
    }
}