﻿using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.Services.Description;

namespace IDEIBiblio.ServiceClient
{
    /* Created based on this blog post http://vnabc.blogspot.pt/2013/03/how-to-add-web-reference-dynamically-in.html and http://stackoverflow.com/questions/8942821/adding-web-reference-dynamically-c-sharp*/
    public class WebServicesClient
    {

        public object CallWebService(string WebServiceUrl, string ServiceName, string UserName, string password, string MethodName, object[] args)
        {

            WebClient Client= new WebClient();

            if (UserName != null)
            {
                Client.Credentials = new NetworkCredential(UserName, password);
            }

            Stream Stream = Client.OpenRead(WebServiceUrl + "?singlewsdl");

            ServiceDescription Description = ServiceDescription.Read(Stream);

            ServiceDescriptionImporter Importer = new ServiceDescriptionImporter();

            Importer.ProtocolName = "Soap";

            Importer.AddServiceDescription(Description,null,null);

            Importer.Style = ServiceDescriptionImportStyle.Client;

            Importer.CodeGenerationOptions = System.Xml.Serialization.CodeGenerationOptions.GenerateProperties;

            CodeNamespace Namespace = new CodeNamespace();

            CodeCompileUnit Unit = new CodeCompileUnit();

            Unit.Namespaces.Add(Namespace);

            ServiceDescriptionImportWarnings Warning = Importer.Import(Namespace, Unit);


            if (Warning == 0)
            {
                CodeDomProvider Provider = CodeDomProvider.CreateProvider("CSharp");

                string[] AssemblyReferences = new string[5] {
                    "System.dll",
                    "System.Web.Services.dll",
                    "System.Web.dll",
                    "System.Xml.dll",
                    "System.Data.dll"
                };

                CompilerParameters Params = new CompilerParameters(AssemblyReferences);

                CompilerResults Results = Provider.CompileAssemblyFromDom(Params, Unit);

                if (Results.Errors.Count > 0)
                {
                    foreach (CompilerError oops in Results.Errors)
                    {

                        System.Diagnostics.Debug.WriteLine("========Compiler error============");

                        System.Diagnostics.Debug.WriteLine(oops.ErrorText);

                    }

                    throw new System.Exception("Compile Error Occured calling webservice. Check Debug ouput window.");

                }

                object WsvcClass = Results.CompiledAssembly.CreateInstance(ServiceName);

                MethodInfo MInfo = WsvcClass.GetType().GetMethod(MethodName);
                return MInfo.Invoke(WsvcClass, args);
            }
            else
            {
                return null;
            }
        }

    }
}