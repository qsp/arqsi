﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IDEIBiblio.ServiceClient.ShippingServices
{
    public class LogisticaSAAdapter  : IShippingServiceAdapter
    {
 
            private const string MethodName = "GetShippingRate";

            private string WebServiceUrl;
            private string ServiceName;

            public void SetUrlAndName(string WebServiceUrl, string ServiceName)
            {
                this.WebServiceUrl = WebServiceUrl;
                this.ServiceName = ServiceName;
            }

            public decimal GetShippingRate(int NumberOfItems)
            {
                //object[] ParamArr = new object[] { NumberOfItems };
                //WebServicesClient WSClient = new WebServicesClient();
                //decimal ShippingRate = (decimal)WSClient.CallWebService(WebServiceUrl, ServiceName, null, null, MethodName, ParamArr);

                LogisticaSAService.LogisticaSAServiceClient proxy = new LogisticaSAService.LogisticaSAServiceClient();

                return (decimal)proxy.GetShippingRate(NumberOfItems);
            }

    }
}