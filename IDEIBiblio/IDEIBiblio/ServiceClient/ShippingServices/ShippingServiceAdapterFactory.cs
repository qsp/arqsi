﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace IDEIBiblio.ServiceClient.ShippingServices
{
    public static class ShippingServiceAdapterFactory
    {

        public static IShippingServiceAdapter CreateShippingServiceAdapter(string ClassName, string WebServiceUrl, string ServiceName)
        {
            string FullyQualifiedClassname = "IDEIBiblio.ServiceClient.ShippingServices." + ClassName;

            Assembly Assembly = Assembly.Load("IDEIBiblio");
            Type Type = Assembly.GetType(FullyQualifiedClassname);

            IShippingServiceAdapter Adapter = (IShippingServiceAdapter)Activator.CreateInstance(Type);
            Adapter.SetUrlAndName(WebServiceUrl, ServiceName);

            return Adapter;
        }

    }
}