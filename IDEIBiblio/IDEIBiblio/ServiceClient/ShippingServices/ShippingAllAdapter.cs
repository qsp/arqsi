﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IDEIBiblio.ServiceClient.ShippingServices
{
    public class ShippingAllAdapter : IShippingServiceAdapter
    {
        private const string MethodName = "GetShippingRate";

        private string WebServiceUrl;
        private string ServiceName;

        public void SetUrlAndName(string WebServiceUrl, string ServiceName)
        {
            this.WebServiceUrl = WebServiceUrl;
            this.ServiceName = ServiceName;
        }

        public decimal GetShippingRate(int NumberOfItems)
        {
            object[] ParamArr = new object[]{NumberOfItems};
            WebServicesClient WSClient = new WebServicesClient();
            float ShippingRate = (float)WSClient.CallWebService(WebServiceUrl, ServiceName, null, null, MethodName, ParamArr);

            return Convert.ToDecimal(ShippingRate);
        }

    }
}