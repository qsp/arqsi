﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDEIBiblio.ServiceClient.ShippingServices
{
    public interface IShippingServiceAdapter
    {

        decimal GetShippingRate(int NumberOfItems);

        void SetUrlAndName(string WebServiceUrl, string ServiceName);

    }
}
