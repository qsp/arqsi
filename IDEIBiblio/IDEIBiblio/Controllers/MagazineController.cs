﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IDEIBiblio.Models;
using IDEIBiblio.AuthorizeAttributes;

namespace IDEIBiblio.Controllers
{
    public class MagazineController : Controller
    {
        private IDEIBiblioDbContext db = new IDEIBiblioDbContext();

        //
        // GET: /Magazine/

        public ActionResult Index()
        {
            ViewBag.Title = "Magazines";
            ViewBag.Message = "Featured:";
            return View(db.Magazines.ToList());
        }

        //
        // GET: /Default1/

        public ActionResult Featured()
        {
            ViewBag.Title = "Magazines";
            ViewBag.Message = "Featured:";
            ICollection<Magazine> Magazines = db.GetMostOrderedMagazines();
            return PartialView(Magazines);
        }

        //
        // GET: /Magazine/Details/5

        public ActionResult Details(int id = 0)
        {
            Magazine magazine = db.Magazines.Find(id);
            if (magazine == null)
            {
                return HttpNotFound();
            }
            ViewBag.Discount = magazine.DiscountPercentage * 100;
            return View(magazine);
        }

        //
        // GET: /Magazine/Create
        [AuthorizeRole]
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Magazine/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Magazine magazine)
        {
            if (ModelState.IsValid)
            {
                db.Products.Add(magazine);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(magazine);
        }

        //
        // GET: /Magazine/Edit/5
        [AuthorizeRole]
        public ActionResult Edit(int id = 0)
        {
            Magazine magazine = db.Magazines.Find(id);
            if (magazine == null)
            {
                return HttpNotFound();
            }
            return View(magazine);
        }

        //
        // POST: /Magazine/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Magazine magazine)
        {
            if (ModelState.IsValid)
            {
                db.Entry(magazine).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(magazine);
        }

        //
        // GET: /Magazine/Delete/5
        [AuthorizeRole]
        public ActionResult Delete(int id = 0)
        {
            Magazine magazine = db.Magazines.Find(id);
            if (magazine == null)
            {
                return HttpNotFound();
            }
            return View(magazine);
        }

        //
        // POST: /Magazine/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Magazine magazine = db.Magazines.Find(id);
            db.Products.Remove(magazine);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}