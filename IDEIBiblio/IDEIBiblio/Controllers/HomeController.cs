﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations.Schema;
using IDEIBiblio.Models;

namespace IDEIBiblio.Controllers
{
    public class HomeController : Controller
    {
        private IDEIBiblioDbContext db = new IDEIBiblioDbContext();

        public ActionResult Index()
        {
            ViewBag.Title = "Welcome!";
            ViewBag.Message = "";
            return View(db.Products.ToList());
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "";

            return View();
        }
    }
}
