﻿using IDEIBiblio.AuthorizeAttributes;
using IDEIBiblio.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace IDEIBiblio.Controllers
{
    public class AdminController : Controller
    {
        IDEIBiblioDbContext db = new IDEIBiblioDbContext();
        //
        // GET: /Admin/

        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("console");
            }
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Index(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid && WebSecurity.Login(model.UserName, model.Password, persistCookie: model.RememberMe))
            {
                return RedirectToAction("console");
            }

            ModelState.AddModelError("", "The user name or password provided is incorrect.");
            return View(model);
        }

        [AuthorizeRole]
        public ActionResult Console()
        {
            ICollection<Role> Roles = db.Roles.ToList();
            ICollection<UserProfile> Users = db.UserProfiles.ToList();
            ICollection<Activity> Activities = db.Activities.ToList();

            ViewBag.Roles = Roles;
            ViewBag.Users = Users;
            ViewBag.Activities = Activities;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UserRoles()
        {
            ICollection<Role> Roles = db.Roles.ToList();
            ICollection<UserProfile> Users = db.UserProfiles.ToList();

            foreach (UserProfile User in Users)
            {
                int UserId = User.UserId;
                string RequestRoles = (Request.Form[UserId.ToString()] != null) ? Request.Form[UserId.ToString()] : "";
                string[] RoleIDs = RequestRoles.Split(',');

                ICollection<UserInRole> UserInRolesToDelete = new List<UserInRole>(User.UserRoles);

                foreach (string RoleIdString in RoleIDs)
                {
                    if (RoleIDs[0].Equals(""))
                    {
                        break;
                    }
                    int RoleId = int.Parse(RoleIdString);
                    Role Role = db.Roles.Find(RoleId);

                    bool IsInList = false;
                    foreach (UserInRole UserInRole in User.UserRoles)
                    {
                        if (UserInRole.Role == Role)
                        {
                            IsInList = true;

                            /*Vai-se manter, logo não é necessário apagar*/
                            UserInRolesToDelete.Remove(UserInRole);
                            break;
                        }
                    }

                    /*Ainda não existe é necessário criar*/
                    if (!IsInList)
                    {
                        UserInRole NewUserInRole = db.UserInRole.Add(new UserInRole
                        {
                            UserID = UserId,
                            RoleID = RoleId
                        });
                    }
                }

                /*Apagar os restantes*/
                foreach (UserInRole UserInRole in UserInRolesToDelete)
                {
                    db.RemoveUserFromRole(UserInRole.UserID, UserInRole.RoleID);
                }

            }

            db.SaveChanges();

            return RedirectToAction("console");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RolesActivities()
        {
            ICollection<Role> Roles = db.Roles.ToList();
            ICollection<Activity> Activities = db.Activities.ToList();

            foreach (Role Role in Roles)
            {
                int RoleId = Role.RoleID;
                string RequestActivities = (Request.Form[RoleId.ToString()] != null) ? Request.Form[RoleId.ToString()] : "";
                string[] ActivitiesIds = RequestActivities.Split(',');

                ICollection<RoleActivity> RoleActivitiesToDelete = new List<RoleActivity>(Role.AuthorizedActions);

                foreach (string ActivityIdString in ActivitiesIds)
                {
                    if (ActivitiesIds[0].Equals(""))
                    {
                        break;
                    }
                    int ActivityId = int.Parse(ActivityIdString);
                    Activity Activity = db.Activities.Find(ActivityId);

                    bool IsInList = false;
                    foreach (RoleActivity RoleActivity in Role.AuthorizedActions)
                    {
                        if (RoleActivity.Activity == Activity)
                        {
                            IsInList = true;

                            /*Vai-se manter, logo não é necessário apagar*/
                            RoleActivitiesToDelete.Remove(RoleActivity);
                            break;
                        }
                    }

                    /*Ainda não existe é necessário criar*/
                    if (!IsInList)
                    {
                        RoleActivity NewRoleActivity = db.RoleActions.Add(new RoleActivity
                        {
                            ActivityID = ActivityId,
                            RoleID = RoleId
                        });
                    }
                }

                /*Apagar os restantes*/
                foreach (RoleActivity RoleActivity in RoleActivitiesToDelete)
                {
                    db.RemoveActivityFromRole(RoleActivity.ActivityID, RoleActivity.RoleID);
                }
            }

            db.SaveChanges();

            return RedirectToAction("console");
        }
    }
}
