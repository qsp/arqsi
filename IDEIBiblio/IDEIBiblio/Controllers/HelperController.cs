﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IDEIBiblio.Models;
using IDEIBiblio.AuthorizeAttributes;
using System.IO;

namespace IDEIBiblio.Controllers
{
    public class HelperController : Controller
    {
        [AuthorizeRole]
        public ActionResult UploadPicture()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult UploadPicture(HttpPostedFileBase file)
        {

            string fileLocation = "/Images/uploads/";
            if (file.ContentLength > 0)
            {
                var fileName = Path.GetFileName(file.FileName);
                var path = Path.Combine(Server.MapPath("~/Images/uploads"), fileName);
                file.SaveAs(path);
                fileLocation += fileName;
            }

            return Json(new { url = fileLocation });
        }
    }
}