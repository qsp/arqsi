﻿using IDEIBiblio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IDEIBiblio.Controllers
{
    public class ShoppingCartController : Controller
    {
        //
        // GET: /ShoppingCart/

        private IDEIBiblioDbContext db = new IDEIBiblioDbContext();

        public ActionResult Edit(int id)
        {
            ShoppingCart Cart = db.ShoppingCarts.Find(id);
            return View(Cart);
        }

        public ActionResult Show()
        {
            string UserName = (User.Identity.IsAuthenticated) ? User.Identity.Name : null;
            string SessionID = HttpContext.Session.SessionID;

            ShoppingCart Cart = db.GetShoppingCart(SessionID, UserName);
            
            return PartialView(Cart);
        }

        public ActionResult ShowAddToCart(int ProductID)
        {
            return PartialView(ProductID);
        }

        public ActionResult ConfirmOrder()
        {
            string UserName = (User.Identity.IsAuthenticated) ? User.Identity.Name : null;
            string SessionID = HttpContext.Session.SessionID;

            ShoppingCart Cart = db.GetShoppingCart(SessionID, UserName);
            TempData["cartid"] = Cart.ShoppingCartID;

            return RedirectToAction("Create", "Order");
        }
 
        [HttpPost]
        public ActionResult AddToCart(int id)
        {
            Product Product= db.Products.Find(id);

            string UserName = (User.Identity.IsAuthenticated) ? User.Identity.Name : null;
            string SessionID = HttpContext.Session.SessionID;
            ShoppingCart Cart = db.GetShoppingCart(SessionID, UserName);

            Cart.AddProductToCart(Product);
            db.SaveChanges();
            
            return PartialView(Cart);
        }

        [HttpPost]
        public ActionResult Remove(int id)
        {
            string UserName = (User.Identity.IsAuthenticated) ? User.Identity.Name : null;
            string SessionID = HttpContext.Session.SessionID;
            ShoppingCart Cart = db.GetShoppingCart(SessionID, UserName);
            
            ShoppingCartLine Line = Cart.ShoppingCartLines.First(line => line.ShoppingCartLineID == id);

            Cart.RemoverFromCart(Line);

            db.SaveChanges();

            var JsonResults = new
            {
                CartCount = Cart.GetTotalNumberOfProducts(),
                CartTotal = Cart.PriceTotal(),
                RemovedId = id
            };
            return Json(JsonResults);
        }
    }
}