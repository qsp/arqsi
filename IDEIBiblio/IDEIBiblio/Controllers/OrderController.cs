﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IDEIBiblio.Models;
using System.EnterpriseServices;
using System.Transactions;
using System.Net.Mail;
using System.Text;

namespace IDEIBiblio.Controllers
{
    public class OrderController : Controller
    {
        private IDEIBiblioDbContext db = new IDEIBiblioDbContext();

        //
        // GET: /Order/

        public ActionResult Index()
        {
            return View(db.Orders.ToList());
        }

        //
        // GET: /Order/Details/5

        public ActionResult Details(int id = 0)
        {
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        //
        // GET: /Order/Create

        [Authorize]
        public ActionResult Create()
        {
            int CartId = (int)TempData["cartid"];
            ShoppingCart Cart = db.ShoppingCarts.Find(CartId);
            if (Cart != null)
            {
                Session["cartid"] = Cart.ShoppingCartID;
            }
            else
            {
                string UserName = User.Identity.Name;
                string SessionID = HttpContext.Session.SessionID;
                Cart = (Session["cart"] != null) ? (ShoppingCart)Session["cart"] : db.GetShoppingCart(SessionID, UserName);
            }
            return View(Cart);
        }

        [Authorize]
        public ActionResult Shipping()
        {
            int CartId = (int)Session["cartid"];
            ShoppingCart Cart = db.ShoppingCarts.Find(CartId);

            if (Cart == null)
            {
                return RedirectToAction("create");
            }

            ICollection<LogisticsCompany> LogisticsCompanies = db.LogisticsCompanies.ToList();
            List<SelectListItem> SelectItems = new List<SelectListItem>();

            foreach (LogisticsCompany Company in LogisticsCompanies)
            {
                SelectItems.Add(new SelectListItem
                {
                    Value = Convert.ToString(Company.LogisticsCompanyID),
                    Text = Company.CompanyName + "(" + Company.GetShippingRate(Cart.GetTotalNumberOfProducts()) + "€)"
                });
            }

            SelectList CompaniesList = new SelectList(SelectItems);

            ViewBag.CompaniesList = SelectItems;

            return View();
        }

        [HttpPost]
        [Authorize]
        public ActionResult ConfirmOrder()
        {
            string UserName = User.Identity.Name;
            UserProfile UserProfile = db.UserProfiles.SingleOrDefault(user => user.UserName == UserName);
            string Address = Request["Address"];
            int CartId = (int)Session["cartid"];
            ShoppingCart Cart = db.ShoppingCarts.Find(CartId);
            
            if (Cart == null)
            {
                return RedirectToAction("create");
            }
                
            /*Create Delivery*/
            int LogCompanyId = Convert.ToInt32(Request.Form["LogisticsCompanyID"]);
            LogisticsCompany LogCompany = db.LogisticsCompanies.Find(LogCompanyId);
            Delivery Delivery = new Delivery
            {
                LogisticsCompanyID = LogCompanyId,
                Price = LogCompany.GetShippingRate(Cart.GetTotalNumberOfProducts())
            };

            Order Order = new Order { 
                UserID = UserProfile.UserId,
                Address = Address,
                Delivery = Delivery
            };
            db.Orders.Add(Order);
            Order.TransferCart(Cart);

            db.SaveChanges();

            SendEmail(UserProfile, Order);
            Session["cartid"] = null;
           

            return View(Order);
        }

        //
        // POST: /Order/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Order order)
        {
            if (ModelState.IsValid)
            {
                order.CreationDate = System.DateTime.Now;
                db.Orders.Add(order);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(order);
        }

        //
        // GET: /Order/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        //
        // POST: /Order/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Order order)
        {
            if (ModelState.IsValid)
            {
                db.Entry(order).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(order);
        }

        //
        // GET: /Order/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        //
        // POST: /Order/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Order order = db.Orders.Find(id);
            db.Orders.Remove(order);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        //
        // GET: /Order/DeleteLine/5

        public ActionResult DeleteLine(int id = 0)
        {
            OrderLine orderLine = db.OrderLines.Find(id);
            if (orderLine == null)
            {
                return HttpNotFound();
            }
            return View(orderLine);
        }

        //
        // POST: /Order/DeleteLine/5

        [HttpPost, ActionName("DeleteLine")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteLineConfirmed(int id)
        {
            OrderLine orderLine = db.OrderLines.Find(id);
            db.OrderLines.Remove(orderLine);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public void SendEmail(UserProfile User, Order Order)
        {
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
            SmtpServer.Credentials = new System.Net.NetworkCredential("bmflavio@gmail.com", "hbbekjrhyshplrbm");
            SmtpServer.Port = 587;
            SmtpServer.EnableSsl = true;

            StringBuilder SBuilder = new StringBuilder();

            SBuilder.Append("Dear " + User.UserName).Append(",\n").Append("\n");
            SBuilder.Append("Your Order:\n\n");

            SBuilder.Append("Product").Append("\t\t\t\t").Append("Qtd").Append("\t\t\t\t").Append("Price").Append("\t\t\t\t").Append("Total").Append("\n\n");
            decimal PriceTotal = 0M;
            foreach (OrderLine line in Order.OrderLines)
            {
                decimal LineTotal = line.UnitaryPrice * line.Quantity;
                PriceTotal += LineTotal;
                SBuilder.Append(line.Product.Name).Append("\t\t\t\t");
                SBuilder.Append(line.Quantity).Append("\t\t\t\t");
                SBuilder.Append(line.UnitaryPrice).Append("\t\t\t\t");
                SBuilder.Append(LineTotal).Append("\n");
            }
            PriceTotal += Order.Delivery.Price;
            SBuilder.Append("\n");
            SBuilder.Append("ShippingCosts").Append("\t\t\t\t").Append("\t\t\t\t").Append("\t\t\t\t").Append(Order.Delivery.Price).Append("\n\n");
            SBuilder.Append("Total").Append("\t\t\t\t").Append("\t\t\t\t").Append("\t\t\t\t").Append("\t\t\t\t").Append(PriceTotal).Append("\n");

            SBuilder.Append("\n\n\n");
            SBuilder.Append("Thank you for the preference");

            mail.From = new MailAddress("bmflavio@gmail.com");
            mail.To.Add(User.UserName);
            mail.Subject = "Order Confirmation " + Order.OrderID;
            mail.Body = SBuilder.ToString();

            SmtpServer.Send(mail);
        }
    }
}