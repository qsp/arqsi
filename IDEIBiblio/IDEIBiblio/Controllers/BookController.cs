﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IDEIBiblio.Models;
using IDEIBiblio.AuthorizeAttributes;
using System.IO;

namespace IDEIBiblio.Controllers
{
    public class BookController : Controller
    {
        private IDEIBiblioDbContext db = new IDEIBiblioDbContext();

        //
        // GET: /Default1/

        public ActionResult Index()
        {
            ViewBag.Title = "Books";
            ViewBag.Message = "Featured:";
            return View(db.Books.ToList());
        }

        //
        // GET: /Default1/

        public ActionResult Featured()
        {
            ViewBag.Title = "Books";
            ViewBag.Message = "Featured:";
            ICollection<Book> books = db.GetMostOrderedBooks();
            return PartialView(books);
        }

        //
        // GET: /Default1/Details/5

        public ActionResult Details(int id = 0)
        {
            Book book = db.Books.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }
            ViewBag.Discount = book.DiscountPercentage * 100;
            return View(book);
        }

        //
        // GET: /Default1/Create
        [AuthorizeRole]
        public ActionResult Create()
        {
            
            SelectList PublisherList = new SelectList(db.Publishers.ToList(), "PublisherID", "Name");
            SelectList AuthorsList = new SelectList(db.Authors.ToList(), "AuthorID", "Name");
            SelectList CategoriesList = new SelectList(db.BookCategories.ToList(), "BookCategoryID", "Name");

            ViewBag.PublisherList = PublisherList;
            ViewBag.AuthorsList = AuthorsList;
            ViewBag.CategoriesList = CategoriesList;

            return View();
        }

        //
        // POST: /Default1/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Book book)
        {
            if (ModelState.IsValid)
            {
                db.Books.Add(book);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(book);
        }

        //
        // GET: /Default1/Edit/5
        //[Authorize(Roles="admin,product_manager")]
        [AuthorizeRole]
        public ActionResult Edit(int id = 0)
        {
            Book book = db.Books.Find(id);


            SelectList PublisherList = new SelectList(db.Publishers.ToList(), "PublisherID", "Name");
            SelectList AuthorsList = new SelectList(db.Authors.ToList(), "AuthorID", "Name");
            SelectList CategoriesList = new SelectList(db.BookCategories.ToList(), "BookCategoryID", "Name");

            ViewBag.PublisherList = PublisherList;
            ViewBag.AuthorsList = AuthorsList;
            ViewBag.CategoriesList = CategoriesList;

            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        //
        // POST: /Default1/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Book book)
        {
//            Book book = db.Books.Create();
            if (ModelState.IsValid)
            {                
//                db.Entry(book).CurrentValues.SetValues(viewbook);
                db.Entry(book).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(book);
        }

        //
        // GET: /Default1/Delete/5
        [AuthorizeRole]
        public ActionResult Delete(int id = 0)
        {
            Book book = db.Books.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        //
        // POST: /Default1/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Book book = db.Books.Find(id);
            db.Books.Remove(book);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [AuthorizeRole]
        public ActionResult CreatePublisher()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult CreatePublisher(Publisher publisher)
        {
            if (ModelState.IsValid)
            {               
                db.Publishers.Add(publisher);
                db.SaveChanges();
                //return RedirectToAction("Index");
            }

            return Json(new { element = "PublisherID", id = publisher.PublisherID, name = publisher.Name });
        }

        [AuthorizeRole]
        public ActionResult CreateAuthor()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult CreateAuthor(Author author)
        {
            if (ModelState.IsValid)
            {
                db.Authors.Add(author);
                db.SaveChanges();
                //return RedirectToAction("Index");
            }

            return Json(new { element = "AuthorID", id = author.AuthorID, name = author.Name });
        }

        [AuthorizeRole]
        public ActionResult CreateCategory()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult CreateCategory(BookCategory category)
        {
            if (ModelState.IsValid)
            {
                db.BookCategories.Add(category);
                db.SaveChanges();
                //return RedirectToAction("Index");
            }

            return Json(new { element = "BookCategoryID", id = category.BookCategoryID, name = category.Name });
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

    }
}