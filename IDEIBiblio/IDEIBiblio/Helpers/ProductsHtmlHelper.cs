﻿using IDEIBiblio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Mvc.Html;
using System.Web.Mvc.Ajax;

namespace IDEIBiblio.Helpers
{
    public static class ProductsHtmlHelper
    {
        private struct ConcreteData
        {
            public string CssClass;
            public string Controller;
            public string HeaderContent;
        }

        public static MvcHtmlString DisplayForProduct(this HtmlHelper HtmlHelper, Product product)
        {
            UrlHelper UrlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            AjaxHelper AjaxHelper = new AjaxHelper(HtmlHelper.ViewContext, HtmlHelper.ViewDataContainer);

            ConcreteData concrete = getConcreteProductContents(product);

            StringBuilder outputHTML = new StringBuilder();

            outputHTML.Append("<div id=\"product_")
                      .Append(product.ProductID)
                      .Append("\" ")
                      .Append("onclick=\"if(event.target.tagName === 'A')return; location.href='")
                      .Append(UrlHelper.Action("Details", concrete.Controller, new { id = product.ProductID }))
                      .Append("';\" ")
                      .Append("class=\"product ")
                      .Append(concrete.CssClass)
                      .Append("\">");

            outputHTML.Append("<div class=\"header\">")
                        .Append("<img alt=\"Product cover\" class=\"image\" src=\"")
                        .Append(product.ImageURL).Append("\" />")
                      .Append("</div>");

            outputHTML.Append("<div class=\"title\">").Append(product.Name).Append("</div>");
            
            outputHTML.Append(concrete.HeaderContent);

            outputHTML.Append("<br><div class=\"price\">").Append(product.DiscountedPrice()).Append("€ </div>");
            outputHTML.Append("<div class=\"links\">").Append(HtmlHelper.Action("showaddtocart", "shoppingcart", new { ProductID = product.ProductID })).Append("</div>");

            outputHTML.Append("</div>");

            return MvcHtmlString.Create(outputHTML.ToString());
        }

        private static ConcreteData getConcreteProductContents(Product product)
        {

            if (product is Book)
                return getConcreteProductContents((Book)product);

            if (product is Magazine)
                return getConcreteProductContents((Magazine)product);

            return new ConcreteData();
        }

        private static ConcreteData getConcreteProductContents(Book book)
        {
            ConcreteData contents = new ConcreteData();

            contents.CssClass = "book";
            contents.Controller = "Book";
            contents.HeaderContent = getConcreteProductHeaderContent(book);

            return contents;
        }

        private static ConcreteData getConcreteProductContents(Magazine magazine)
        {
            ConcreteData contents = new ConcreteData();

            contents.CssClass = "magazine";
            contents.Controller = "Magazine";
            contents.HeaderContent = getConcreteProductHeaderContent(magazine);

            return contents;
        }

        private static string getConcreteProductHeaderContent(Book book)
        {
            StringBuilder outputHTML = new StringBuilder();

            //outputHTML.Append("<div class=\"isbn\">").Append(book.Isbn).Append("</div>");
            outputHTML.Append("<div class=\"author\">").Append(book.Author.Name).Append("</div>");

            return outputHTML.ToString();
        }

        private static string getConcreteProductHeaderContent(Magazine magazine)
        {
            StringBuilder outputHTML = new StringBuilder();

            outputHTML.Append("<div class=\"edition\">Edition #").Append(magazine.EditionNumber).Append("</div>");
            //outputHTML.Append("<div class=\"publishing_date\">").Append(magazine.PublishingDate.Date).Append("</div>");

            return outputHTML.ToString();
        }

    }
}