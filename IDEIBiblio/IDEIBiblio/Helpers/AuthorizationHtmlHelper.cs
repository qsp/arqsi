﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Mvc.Html;

namespace IDEIBiblio.Helpers
{
    /*
     * Baseado e adaptado deste thread do stackoverflow http://stackoverflow.com/questions/2721869/security-aware-action-link
     */
    public static class AuthorizationHtmlHelper
    {
        public static MvcHtmlString AuthorizedActionLink(this HtmlHelper HtmlHelper, string LinkName, string ActionName)
        {
            return HtmlHelper.HasActionPermission(ActionName, "") ? HtmlHelper.ActionLink(LinkName, ActionName) : MvcHtmlString.Create("");
        }

        public static MvcHtmlString AuthorizedActionLink(this HtmlHelper HtmlHelper, string LinkName, string ActionName, RouteValueDictionary RouteValueDictionary)
        {
            return HtmlHelper.HasActionPermission(ActionName, "") ? HtmlHelper.ActionLink(LinkName, ActionName, RouteValueDictionary) : MvcHtmlString.Create("");
        }

        public static MvcHtmlString AuthorizedActionLink(this HtmlHelper HtmlHelper, string LinkName, string ActionName, object HtmlObjects)
        {
            return HtmlHelper.HasActionPermission(ActionName, "") ? HtmlHelper.ActionLink(LinkName, ActionName, HtmlObjects) : MvcHtmlString.Create("");
        }

        public static MvcHtmlString AuthorizedActionLink(this HtmlHelper HtmlHelper, string LinkName, string ActionName, RouteValueDictionary RouteValueDictionary, object HtmlObjects)
        {
            return HtmlHelper.HasActionPermission(ActionName, "") ? HtmlHelper.ActionLink(LinkName, ActionName, RouteValueDictionary, HtmlObjects) : MvcHtmlString.Create("");
        }

        public static MvcHtmlString AuthorizedActionLink(this HtmlHelper HtmlHelper, string LinkName, string ActionName, string ControllerName)
        {
            return HtmlHelper.HasActionPermission(ActionName, ControllerName) ? HtmlHelper.ActionLink(LinkName, ActionName, ControllerName) : MvcHtmlString.Create("");
        }
    } 
}