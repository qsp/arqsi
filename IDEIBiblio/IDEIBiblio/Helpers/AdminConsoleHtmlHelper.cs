﻿using IDEIBiblio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace IDEIBiblio.Helpers
{
    public static class AdminConsoleHtmlHelper
    {

        public static MvcHtmlString UsersAndRolesTable(this HtmlHelper HtmlHelper, ICollection<UserProfile> Users, ICollection<Role> Roles)
        {
            StringBuilder HTML = new StringBuilder();
                
            HTML.Append("<table>");

            /*Table Headers*/
            HTML.Append("<tr>").Append("<th>Users</th>");
            foreach (Role Role in Roles)
            {
                string RoleName = Role.RoleName;
                HTML.Append("<th>" + RoleName + "</th>");
            }
            HTML.Append("</tr>");

            foreach (UserProfile User in Users)
            {
                HTML.Append("<tr>");
                HTML.Append("<td>" + User.UserName + "</td>");
                foreach (Role Role in Roles)
                {
                    bool IsUserInRole = false;
                    foreach (UserInRole UserInRole in User.UserRoles)
                    {
                        if (UserInRole.Role.RoleName == Role.RoleName)
                        {
                            IsUserInRole = true;
                        }
                    }
                    HTML.Append("<fieldset name='" + User.UserId + "'>");
                    if (IsUserInRole)
                    {
                        HTML.Append("<td><input type='checkbox'  name='" + User.UserId + "'[] value='" + Role.RoleID + "' checked/></td>");
                    }
                    else
                    {
                        HTML.Append("<td><input type='checkbox' name='" + User.UserId + "'[] value='" + Role.RoleID + "'/></td>");
                    }
                    HTML.Append("</fieldset>");
                }
                HTML.Append("</tr>");
            }

            HTML.Append("</table>");

            return new MvcHtmlString(HTML.ToString());
        }

        public static MvcHtmlString ActivityAuthorizationTable(this HtmlHelper HtmlHelper, ICollection<Role> Roles, ICollection<Activity> Activities)
        {
            StringBuilder HTML = new StringBuilder();

            HTML.Append("<table>");

            /*Table Headers*/
            HTML.Append("<tr>").Append("<th>Activities</th>");
            foreach (Role Role in Roles)
            {
                string RoleName = Role.RoleName;
                HTML.Append("<th>" + RoleName + "</th>");
            }
            HTML.Append("</tr>");

            foreach (Activity Activity in Activities)
            {
                HTML.Append("<tr>");
                HTML.Append("<td>" + Activity.Controller + " - " + Activity.Action + "</td>");
                foreach (Role Role in Roles)
                {
                    bool DoesRoleHaveActivity = false;
                    foreach (RoleActivity RoleActivity in Role.AuthorizedActions)
                    {
                        if (RoleActivity.ActivityID == Activity.ID)
                        {
                            DoesRoleHaveActivity = true;
                        }
                    }
                    HTML.Append("<fieldset name='" + Activity.ID + "'>");
                    if (DoesRoleHaveActivity)
                    {
                        HTML.Append("<td><input type='checkbox'  name='" + Role.RoleID + "'[] value='" + Activity.ID + "' checked/></td>");
                    }
                    else
                    {
                        HTML.Append("<td><input type='checkbox' name='" + Role.RoleID + "'[] value='" + Activity.ID + "'/></td>");
                    }
                    HTML.Append("</fieldset>");
                }
                HTML.Append("</tr>");
            }

            HTML.Append("</table>");

            return new MvcHtmlString(HTML.ToString());
        }

    }
}