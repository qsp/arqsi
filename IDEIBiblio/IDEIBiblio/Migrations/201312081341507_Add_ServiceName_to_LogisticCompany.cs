namespace IDEIBiblio.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_ServiceName_to_LogisticCompany : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LogisticsCompanies", "ServiceName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.LogisticsCompanies", "ServiceName");
        }
    }
}
