namespace IDEIBiblio.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Mantain_UserName_as_ColumnName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserProfile", "UserName", c => c.String());
            DropColumn("dbo.UserProfile", "UserEmail");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserProfile", "UserEmail", c => c.String());
            DropColumn("dbo.UserProfile", "UserName");
        }
    }
}
