namespace IDEIBiblio.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ProductID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        ImageURL = c.String(),
                        CurrentPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        EditionNumber = c.Int(),
                        PublishingDate = c.DateTime(),
                        Isbn = c.String(),
                        PublishingDate1 = c.DateTime(),
                        AuthorID = c.Int(),
                        PublisherID = c.Int(),
                        BookCategoryID = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.ProductID)
                .ForeignKey("dbo.Authors", t => t.AuthorID, cascadeDelete: true)
                .ForeignKey("dbo.Publishers", t => t.PublisherID, cascadeDelete: true)
                .ForeignKey("dbo.BookCategories", t => t.BookCategoryID, cascadeDelete: true)
                .Index(t => t.AuthorID)
                .Index(t => t.PublisherID)
                .Index(t => t.BookCategoryID);
            
            CreateTable(
                "dbo.Authors",
                c => new
                    {
                        AuthorID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.AuthorID);
            
            CreateTable(
                "dbo.Publishers",
                c => new
                    {
                        PublisherID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.PublisherID);
            
            CreateTable(
                "dbo.BookCategories",
                c => new
                    {
                        BookCategoryID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.BookCategoryID);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        OrderID = c.Int(nullable: false, identity: true),
                        UserID = c.Int(nullable: false),
                        CreationDate = c.DateTime(nullable: false),
                        Address = c.String(),
                        DeliveryID = c.Int(),
                    })
                .PrimaryKey(t => t.OrderID)
                .ForeignKey("dbo.UserProfile", t => t.UserID, cascadeDelete: true)
                .ForeignKey("dbo.Deliveries", t => t.DeliveryID)
                .Index(t => t.UserID)
                .Index(t => t.DeliveryID);
            
            CreateTable(
                "dbo.OrderLines",
                c => new
                    {
                        OrderLineID = c.Int(nullable: false, identity: true),
                        OrderID = c.Int(nullable: false),
                        ProductID = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                        UnitaryPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.OrderLineID)
                .ForeignKey("dbo.Orders", t => t.OrderID, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductID, cascadeDelete: true)
                .Index(t => t.OrderID)
                .Index(t => t.ProductID);
            
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.UserInRoles",
                c => new
                    {
                        UserInRoleID = c.Int(nullable: false, identity: true),
                        UserID = c.Int(nullable: false),
                        RoleID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UserInRoleID)
                .ForeignKey("dbo.UserProfile", t => t.UserID, cascadeDelete: true)
                .ForeignKey("dbo.Roles", t => t.RoleID, cascadeDelete: true)
                .Index(t => t.UserID)
                .Index(t => t.RoleID);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        RoleID = c.Int(nullable: false, identity: true),
                        RoleName = c.String(),
                    })
                .PrimaryKey(t => t.RoleID);
            
            CreateTable(
                "dbo.RoleActivities",
                c => new
                    {
                        RoleActivityID = c.Int(nullable: false, identity: true),
                        RoleID = c.Int(nullable: false),
                        ActivityID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.RoleActivityID)
                .ForeignKey("dbo.Roles", t => t.RoleID, cascadeDelete: true)
                .ForeignKey("dbo.Activities", t => t.ActivityID, cascadeDelete: true)
                .Index(t => t.RoleID)
                .Index(t => t.ActivityID);
            
            CreateTable(
                "dbo.Activities",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Controller = c.String(),
                        Action = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Deliveries",
                c => new
                    {
                        DeliveryID = c.Int(nullable: false, identity: true),
                        LogisticsCompanyID = c.Int(nullable: false),
                        TrackingNo = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        EstimatedDeliveryDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.DeliveryID);
            
            CreateTable(
                "dbo.LogisticsCompanies",
                c => new
                    {
                        LogisticsCompanyID = c.Int(nullable: false, identity: true),
                        WebServiceURL = c.String(),
                        LogisticsAdapterClass = c.String(),
                    })
                .PrimaryKey(t => t.LogisticsCompanyID);
            
            CreateTable(
                "dbo.ShoppingCarts",
                c => new
                    {
                        ShoppingCartID = c.Int(nullable: false, identity: true),
                        UserID = c.Int(),
                        SessionID = c.String(),
                        LastUpdate = c.DateTime(),
                    })
                .PrimaryKey(t => t.ShoppingCartID)
                .ForeignKey("dbo.UserProfile", t => t.UserID)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.ShoppingCartLines",
                c => new
                    {
                        ShoppingCartLineID = c.Int(nullable: false, identity: true),
                        ShoppingCartID = c.Int(),
                        ProductID = c.Int(),
                        Quantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ShoppingCartLineID)
                .ForeignKey("dbo.ShoppingCarts", t => t.ShoppingCartID)
                .ForeignKey("dbo.Products", t => t.ProductID)
                .Index(t => t.ShoppingCartID)
                .Index(t => t.ProductID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.ShoppingCartLines", new[] { "ProductID" });
            DropIndex("dbo.ShoppingCartLines", new[] { "ShoppingCartID" });
            DropIndex("dbo.ShoppingCarts", new[] { "UserID" });
            DropIndex("dbo.RoleActivities", new[] { "ActivityID" });
            DropIndex("dbo.RoleActivities", new[] { "RoleID" });
            DropIndex("dbo.UserInRoles", new[] { "RoleID" });
            DropIndex("dbo.UserInRoles", new[] { "UserID" });
            DropIndex("dbo.OrderLines", new[] { "ProductID" });
            DropIndex("dbo.OrderLines", new[] { "OrderID" });
            DropIndex("dbo.Orders", new[] { "DeliveryID" });
            DropIndex("dbo.Orders", new[] { "UserID" });
            DropIndex("dbo.Products", new[] { "BookCategoryID" });
            DropIndex("dbo.Products", new[] { "PublisherID" });
            DropIndex("dbo.Products", new[] { "AuthorID" });
            DropForeignKey("dbo.ShoppingCartLines", "ProductID", "dbo.Products");
            DropForeignKey("dbo.ShoppingCartLines", "ShoppingCartID", "dbo.ShoppingCarts");
            DropForeignKey("dbo.ShoppingCarts", "UserID", "dbo.UserProfile");
            DropForeignKey("dbo.RoleActivities", "ActivityID", "dbo.Activities");
            DropForeignKey("dbo.RoleActivities", "RoleID", "dbo.Roles");
            DropForeignKey("dbo.UserInRoles", "RoleID", "dbo.Roles");
            DropForeignKey("dbo.UserInRoles", "UserID", "dbo.UserProfile");
            DropForeignKey("dbo.OrderLines", "ProductID", "dbo.Products");
            DropForeignKey("dbo.OrderLines", "OrderID", "dbo.Orders");
            DropForeignKey("dbo.Orders", "DeliveryID", "dbo.Deliveries");
            DropForeignKey("dbo.Orders", "UserID", "dbo.UserProfile");
            DropForeignKey("dbo.Products", "BookCategoryID", "dbo.BookCategories");
            DropForeignKey("dbo.Products", "PublisherID", "dbo.Publishers");
            DropForeignKey("dbo.Products", "AuthorID", "dbo.Authors");
            DropTable("dbo.ShoppingCartLines");
            DropTable("dbo.ShoppingCarts");
            DropTable("dbo.LogisticsCompanies");
            DropTable("dbo.Deliveries");
            DropTable("dbo.Activities");
            DropTable("dbo.RoleActivities");
            DropTable("dbo.Roles");
            DropTable("dbo.UserInRoles");
            DropTable("dbo.UserProfile");
            DropTable("dbo.OrderLines");
            DropTable("dbo.Orders");
            DropTable("dbo.BookCategories");
            DropTable("dbo.Publishers");
            DropTable("dbo.Authors");
            DropTable("dbo.Products");
        }
    }
}
