namespace IDEIBiblio.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserName_To_UserEmail : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserProfile", "UserEmail", c => c.String());
            DropColumn("dbo.UserProfile", "UserName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserProfile", "UserName", c => c.String());
            DropColumn("dbo.UserProfile", "UserEmail");
        }
    }
}
