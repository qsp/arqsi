namespace IDEIBiblio.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Name_to_Logistics_Company : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LogisticsCompanies", "CompanyName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.LogisticsCompanies", "CompanyName");
        }
    }
}
