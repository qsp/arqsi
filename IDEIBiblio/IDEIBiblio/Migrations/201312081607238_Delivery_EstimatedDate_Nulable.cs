namespace IDEIBiblio.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Delivery_EstimatedDate_Nulable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Deliveries", "EstimatedDeliveryDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Deliveries", "EstimatedDeliveryDate", c => c.DateTime(nullable: false));
        }
    }
}
