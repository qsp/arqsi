namespace IDEIBiblio.Migrations
{
    using IDEIBiblio.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using WebMatrix.WebData;

    internal sealed class Configuration : DbMigrationsConfiguration<IDEIBiblio.Models.IDEIBiblioDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(IDEIBiblio.Models.IDEIBiblioDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            //WebSecurity.InitializeDatabaseConnection(
            //    "DefaultConnection",
            //    "UserProfile",
            //    "UserId",
            //    "UserName", autoCreateTables: true);

            var authors = new List<Author>
            {
                new Author { Name = "Martin J Fowler" },
                new Author { Name = "Donna Tartt" }
            };

            authors.ForEach(s => context.Authors.AddOrUpdate(p => p.Name, s));
            context.SaveChanges();

            var publishers = new List<Publisher>
            {   
                new Publisher { Name = "Addison-Wesley"},
                new Publisher {Name="Little, Brown and Company"}
            };
            publishers.ForEach(s => context.Publishers.AddOrUpdate(p => p.Name, s));
            context.SaveChanges();

            var bookcategories = new List<BookCategory>
            {
                new BookCategory { Name = "Software"},
                new BookCategory { Name = "Fiction"}
            };
            bookcategories.ForEach(s => context.BookCategories.AddOrUpdate(p => p.Name, s));
            context.SaveChanges();



            var products = new List<Product> 
            {
                new Book
                {
                    Name = "UML Distilled",
                    Description = "A guide to using UML describes major UML diagrams, their creation, and how to decipher them.",
                    ImageURL = "http://bks0.books.google.pt/books?id=nHZslSr1gJAC&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api",
                    Isbn = "9780321193681",
                    CurrentPrice = 22.41M,                    
                    PublishingDate = new DateTime(2003, 9, 1),
                    AuthorID = authors.Single( i => i.Name == "Martin J Fowler").AuthorID,
                    PublisherID = publishers.Single( i => i.Name == "Addison-Wesley").PublisherID,
                    BookCategoryID = bookcategories.Single( i => i.Name == "Software").BookCategoryID
                },
                new Book
                {
                    Name = "The Goldfinch",
                    Description = "\"The Goldfinch is a rarity that comes along perhaps half a dozen times per decade, a smartly written literary novel that connects with the heart as well as the mind....Donna Tartt has delivered an extraordinary work of fiction.\"--Stephen King, The New York Times Product Review",
                    ImageURL = "http://bks7.books.google.pt/books?id=T2CA83gbtM8C&printsec=frontcover&img=1&zoom=1&edge=curl&imgtk=AFLRE715P15o98dQO2w3swXVsoN-K9UcKwMonl7VtiHyco0QMrCyoR1D4EBUjH-dcw58bYrUXTSv8_JFACuVk2xSUm1fAt7X2to05P9OXYHz1UOlEWx6Pf772B6J7HZT-LLYBSoxAxf3",
                    Isbn = "9781405529518",
                    CurrentPrice = 15.41M,
                    PublishingDate = new DateTime(2013, 10, 22),
                    AuthorID = authors.Single( i => i.Name == "Donna Tartt").AuthorID,
                    PublisherID = publishers.Single( i => i.Name == "Little, Brown and Company").PublisherID,
                    BookCategoryID = bookcategories.Single( i => i.Name == "Fiction").BookCategoryID
                },
                new Magazine
                {
                    Name = "Cosmopolitan",
                    Description = "When you're craving love, sex, fashion and beauty advice that's fun, fearless and custom created for your gutsy, sexy life, nothing but Cosmopolitan will do.",
                    ImageURL = "http://ecx.images-amazon.com/images/I/619jg3VOBRL._SY300_.jpg",
                    CurrentPrice = 1.17M,
                    PublishingDate = new DateTime(2003, 9, 1)
                },
                new Magazine
                {
                    Name = "Men's Health",
                    Description = "Men's Health magazine contains daily tips and articles on fitness, nutrition, relationships, sex, career and lifestyle.",
                    ImageURL = "http://bks1.books.google.pt/books?id=VMgDAAAAMBAJ&printsec=frontcover&img=1&zoom=1&edge=curl&imgtk=AFLRE720krHySbzTx29v4Vs5PjfWgypyjhwPczPeCz0Of4s0cFbIIxTAJonwuapiTqqFzFHVxPr0R-A4lYq6HxUwwykJTqhz_CO7I7c56tEHziI9YMkh_vgsrZm19F0U8ZDnh24iz7oc",
                    CurrentPrice = 2.49M,
                    PublishingDate = new DateTime(2013, 10, 22)
                }
            };

            products.ForEach(s => context.Products.AddOrUpdate(p => p.Name, s));
            context.SaveChanges();


            //Adicionar Roles (Admin,Product_Manager e Client)
            var activities = new List<Activity> 
            {
                new Activity
                {
                Controller = "admin",
                Action = "console"
                },
                new Activity
                {
                Controller = "book",
                Action = "edit"
                },

                new Activity
                {
                Controller = "book",
                Action = "create"
                },

                new Activity
                {
                    Controller = "magazine",
                    Action = "edit"
                },

                new Activity
                {
                Controller = "magazine",
                Action = "create"
                }
            };

            activities.ForEach(s => context.Activities.AddOrUpdate(p => new { p.Controller, p.Action}, s));
            context.SaveChanges();
                  
            var roles = new List<Role> 
            {
                new Role
                {
                    RoleName = "Admin"
                },

                new Role
                {
                    RoleName = "Product Manager"
                },
            
                new Role
                {
                    RoleName = "Client"
                }
            };

            roles.ForEach(s => context.Roles.AddOrUpdate(p => p.RoleName, s));
            context.SaveChanges();

            var roleactivities = new List<RoleActivity>
            {
                new RoleActivity
                {
                RoleID = roles.Single(i => i.RoleName == "Admin").RoleID, 
                ActivityID = activities.Single(i => i.Controller == "admin" && i.Action == "console").ID
                }
            };

            roleactivities.ForEach(s => context.RoleActions.AddOrUpdate(p => new { p.RoleID, p.ActivityID }, s));
            context.SaveChanges();

            //Add logistic companies:
            var logisticCompanies = new List<LogisticsCompany> 
            {
                new LogisticsCompany
                {
                    CompanyName = "ShippingAll",
                    LogisticsAdapterClass = "ShippingAllAdapter",
                    ServiceName = "shippingallservice",
                    WebServiceURL = "http://uvm095.dei.isep.ipp.pt:8080/shippingallservice.php"
                },

                new LogisticsCompany
                {
                    CompanyName = "LogisticaSA",
                    LogisticsAdapterClass = "LogisticaSAAdapter",
                    ServiceName = "LogisticaSAService",
                    WebServiceURL = "http://wvm095.dei.isep.ipp.pt:8080/LogisticaSAService.svc"
                }
            };
            logisticCompanies.ForEach(s => context.LogisticsCompanies.AddOrUpdate(p => p.CompanyName, s));
            context.SaveChanges();

        }
    }
}
